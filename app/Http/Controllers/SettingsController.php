<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
class SettingsController extends BaseController {

 
    /**
     * handle data posted by ajax request
     */
    public function create() {

        $id = \Auth::user()->id;
        $term = \Session::get('term');
        
        $subject_id = \Session::get('subject_id');
        $act_name = \Input::get('deletable');
        \DB::table('selected_activities')->where('load_id',$subject_id)->where('act_name',$act_name)->delete();
        \DB::table('grades')->where('records_enrollment_faculty_load_id',$subject_id)->where('name',$act_name)->delete();
        \DB::table('activities')->join('records','records.id','=','activities.records_id')->where('records.enrollment_faculty_load_id',$subject_id)->where('activities.act_name','LIKE',$act_name.'%')->delete();
        $response = 'The activity named '.$act_name.' has been deleted.';

        return \Response::json( $response );
    }
 
}