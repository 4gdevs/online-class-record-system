<?php

namespace App\Http\Controllers;

use App\Http\Controllers\UsersController;
use Illuminate\Http\Request;

class MainController extends UsersController {

    public function getPrint($id, $term) {
        // $pdf = \App::make('dompdf.wrapper');
        $ActivitiesData = $this->getCombineData($id, $term);
        // dd($ActivitiesData);
        $pdf = \App::make('dompdf.wrapper');
        $html = '<html><body>-----------------';
        foreach ($ActivitiesData[1] as $data) {
            $html.= '<table><tr><th>'
                    . $data['act_name']
                    . '  :  '
                    . $data['score']
                    . '/'
                    . $data['total']
                    . '</th> </tr></table>';
        }
        $html .= '-----------------</body></html>';
        $pdf->loadHTML($html)->setPaper('a4')->setOrientation('portrait')->setWarnings(false)->save('myfile.pdf');
        return $pdf->stream();

        /*   $pdf->loadHTML(''
          . ''
          . '<h1 style = "text-align:center;">Class Record</h1>'
          .'<table>'
          . ' <tr>'
          foreach($ActivitiesData[1] as $data){
          .'<th>'.$data['act_name']
          . '</th>'
          }
          . '</tr>'
          . '</table>'
          )->setPaper('a4')->setOrientation('portrait')->setWarnings(false)->save('myfile.pdf');


          return $pdf->stream(); */
    }

    public function getMidterm($id) {
        $term = 'Midterm';
        // dd($id);
        $studentinfo = \DB::table('students')->join('enrollment', 'enrollment.students_id', '=', 'students.id')
                        ->select('students.sname', 'students.id')
                        ->where('enrollment.faculty_load_id', $id)->get();
        $studentInfo1 = json_decode(json_encode($studentinfo), true);

        foreach ($studentInfo1 as $key => $data) {
            $studentInfo[$data['id']] = $data;
        }

        $studentGrades1 = \DB::table('students')->join('enrollment', 'enrollment.students_id', '=', 'students.id')
                        ->join('records', 'records.enrollment_id','=', 'enrollment.id')
                        ->select('records.*')
                        ->where('records.term', $term)
                        ->where('enrollment.faculty_load_id', $id)->get();
        $studentGrades1 = json_decode(json_encode($studentGrades1), true);
        
        foreach($studentGrades1 as $key1 => $data) {
            $studentGrades[$data['enrollment_students_id']] = $data;
        }
        
        $query = \DB::table('records')->where('enrollment_faculty_load_id',$id)->where('term',$term)->get();
        $reclist1 = json_decode(json_encode($query), true);

        foreach ($reclist1 as $key => $data) {
            $reclist[$data['enrollment_students_id']] = $data;
        }

        foreach ($reclist1 as $key => $data) {
            $query = \DB::table('activities')->where('records_id',$data['id'])->get();
            $actlist1 = json_decode(json_encode($query), true);
            $actlist2[$data['enrollment_students_id']] = $actlist1;
        }
        
        foreach ($actlist2 as $key => $data) {
            $count = 0;
            foreach ($data as $key2 => $data2) {
                $actlist[$key][$data2['act_name']] = $data[$count];
                $actlist[$key][$data2['act_name']][$studentInfo[$key]['sname']] = $studentInfo[$key]['sname'];
                $actlist[$key][$data2['act_name']][$studentInfo[$key]['id']] = $studentInfo[$key]['id'];
                $count++;
            }
            $actlist[$key][$reclist[$key]['term']] = $reclist[$key];
            $actlist[$key][$reclist[$key]['term']][$studentInfo[$key]['sname']] = $studentInfo[$key]['sname'];
            $actlist[$key][$reclist[$key]['term']][$studentInfo[$key]['id']] = $studentInfo[$key]['id'];
        }
        //dd($actlist);
    /*    
        foreach($studentAct1 as $key1 => $data) {
            $studentAct[$data['student_id']] = $data;
        }
        
        dd($studentAct);
        */
        return view('pages.chairman.browse_midterm')->with('studentrecords', $actlist);
    }

    public function postSubmit($id) {
        if (\Auth::check()) {
            $gradesData = $this->getGradesData($id);
            // dd($gradesData);
        } else {
            return \Redirect::to('/login');
        }
    }

    public function postRemove($id) {
        $load_id = \Input::get('load_id');
        \DB::table('enrollment')->where('students_id', $id)->where('faculty_load_id', $load_id)->delete();
        return \Redirect::back();
    }

    public function getEnrollstudent($id) {
        $studentData = null;
        return view('pages.instructor.browse_enroll')
                        ->with('studentData', $studentData)
                        ->with('id', $id);
    }

    public function postEnrollstudents($id) {
        $data = \Input::all();
        $count = 0;
        foreach ($data as $data2) {
            $dataeye[] = $data2;
            $count++;
        }
        //dd($dataeye);
        for ($i = 1; $i < $count; $i++) {


            // dd($data1);
            \DB::insert('insert into enrollment (faculty_load_id,students_id) values (?,?)', array($id, $dataeye[$i]));
            $enrolment_id = $this->getEnrolmentID($id, $dataeye[$i]);
            //  dd($enrolment_id);
            \DB::insert('insert into overall (enrollment_id,enrollment_faculty_load_id,enrollment_students_id) values ( ?,?,?)', array($enrolment_id[0]['id'], $id, $dataeye[$i]));

            for ($j = 0; $j < 2; $j++) {
                if ($j == 0) {
                    $term = 'Midterm';
                    \DB::insert('insert into records (term,enrollment_faculty_load_id,enrollment_students_id,exam_total,enrollment_id) values ( ?,?,?,?,?)'
                            , array($term, $id, $dataeye[$i], 100, $enrolment_id[0]['id']));

                    $record_id = $this->getRecordId($term, $id, $dataeye[$i]);

                    \DB::insert('insert into activities (records_id,act_name,total,type) values ( ?,?,?,?)'
                            , array($record_id[0]['id'], 'Quiz1', 100, 'lec'));
                    \DB::insert('insert into grades (records_id,records_enrollment_students_id,records_enrollment_faculty_load_id,name,records_enrollment_id) values (?, ?,?,?,?)'
                            , array($record_id[0]['id'], $dataeye[$i], $id, 'Quiz', $enrolment_id[0]['id']));
                } else if ($j == 1) {
                    $term = 'Pre-Final';
                    \DB::insert('insert into records (term,enrollment_faculty_load_id,enrollment_students_id,exam_total,enrollment_id) values ( ?,?,?,?,?)'
                            , array($term, $id, $dataeye[$i], 100, $enrolment_id[0]['id']));

                    $record_id = $this->getRecordId($term, $id, $dataeye[$i]);
                    \DB::insert('insert into activities (records_id,act_name,total,type) values ( ?,?,?,?)'
                            , array($record_id[0]['id'], 'Quiz1', 100, 'lec'));

                    \DB::insert('insert into grades (records_id,records_enrollment_students_id,records_enrollment_faculty_load_id,name,records_enrollment_id) values (?, ?,?,?,?)'
                            , array($record_id[0]['id'], $dataeye[$i], $id, 'Quiz', $enrolment_id[0]['id']));
                    //  dd();
                }
            }
            \DB::table('faculty_load')->where('id', $id)->update(array('setup_flag' => 'Y'));
        }
        return \Redirect::to('/instructor');
    }

    public function postFetchdata($id) {
        $data = \Input::all();
        $course = $data['searchkey'];
        $results = \DB::select(\DB::raw("SELECT * FROM students WHERE course like '%$course%' Order BY sname asc"));
        $studentData = json_decode(json_encode($results), true);
        return view('pages.instructor.browse_enroll')
                        ->with('studentData', $studentData)
                        ->with('id', $id);
    }

    public function getEditclass($id) {
        $studentEnrolledData1 = \DB::table('students')->join('enrollment', 'enrollment.students_id', '=', 'students.id')
                        ->join('faculty_load', 'enrollment.faculty_load_id', '=', 'faculty_load.id')
                        ->select('students.*', 'faculty_load.class', 'faculty_load.id as load_id', 'faculty_load.subject')->where('enrollment.faculty_load_id', $id)->OrderBy('sname', 'asc')->get();
        $studentEnrolledData = json_decode(json_encode($studentEnrolledData1), true);
        return view('pages.instructor.browse_edit_class')
                        ->with('studentEnrolledData', $studentEnrolledData)
                        ->with('id', $id);
    }

    public function getIndex() {
        if (\Auth::check()) {
            //  $userData[] =  \Auth::user();
            //    dd($userData);
            $user = \Auth::user()->userType;
            $department = \Auth::user()->department;
            $college = \Auth::user()->college;
            if ($user == 'Instructor') {
                return \Redirect::to('/instructor');
            }
            if ($user == 'Admin') {
                return \Redirect::to('/admin');
            }
            if ($user == 'Dean') {
                return \Redirect::to('/dean/' . $college);
            }
            if ($user == 'Chairman') {
                return \Redirect::to('/chairman/' . $department);
            }
            // return View('pages.mainpages.browse_chairman_file');
        } else {
            return \Redirect::to('/login');
        }
    }

    public function getChairman() {
        if (\Auth::check()) {
            $department = \Auth::user()->department;
            $user = \Auth::user()->userType;
            $instructorList = $this->generateInstructor($department);
            if ($user == 'Chairman') {
                // dd($instructorList);
                return view('pages.chairman.browse_chairman_file')->with('instructors', $instructorList)->with('department', $department);
            } else {

                die('Your not allowed to view this page');
            }
        } else {
            return \Redirect::to('/login');
        }
    }

    function getClasslist($id) {
        if (\Auth::check()) {
            $getSubject = $this->generateInstructorSubject($id);
            //  dd($getSubject);
            $getInstructorData = $this->generateInstructorData($id);
            $term = 'Midterm';
            $user = \Auth::user()->userType;
            $loadSY = $this->getLoadSY($id);
            $gradesData = $this->getGradesData1($id);
            foreach ($getSubject as $data) {
                $activitiesData[] = $this->getActivitiesData($data['id'], 'Midterm');
                $activitiesData = array_column($activitiesData, 0);
            }
            if ($user == 'Chairman') {
                return view('pages.chairman.browse_classlist')->with('subjectData', $getSubject)
                                ->with('instructorData', $getInstructorData)
                                ->with('load', $loadSY)
                                ->with('gradesData', $gradesData);
            }
            if ($user == 'Dean') {
                return view('pages.dean.browse_classlist')->with('subjectData', $getSubject)
                                ->with('instructorData', $getInstructorData)
                                ->with('load', $loadSY)
                                ->with('gradesData', $gradesData);
            }
            return \Redirect::to('/login');
        }
    }

    function postClasslist($id) {
        if (\Auth::check()) {
            $schoolYear = \Input::get('schoolYear');
            \Session::put('schoolYear', $schoolYear);
            $semester = \Input::get('semester');
            \Session::put('semester', $semester);
            $getSubject = $this->generateSubject($schoolYear, $semester, $id);
            //  dd($getSubject);
            $getInstructorData = $this->generateInstructorData($id);
            $user = \Auth::user()->userType;
            $loadSY = $this->getLoadSY($id);
            $gradesData = $this->getGradesData1($id);
            /* foreach($getSubject as $subject){
              $Load_ID = \DB::table('faculty_load')->select('
              $gradesData = $this->getGradesData1($id);
              }
             * */
            // dd($subject1);

            if ($user == 'Chairman') {
                return view('pages.chairman.browse_classlist')->with('subjectData', $getSubject)
                                ->with('instructorData', $getInstructorData)
                                ->with('load', $loadSY)
                                ->with('gradesData', $gradesData);
            }
            if ($user == 'Dean') {
                return view('pages.dean.browse_classlist')->with('subjectData', $getSubject)
                                ->with('instructorData', $getInstructorData)
                                ->with('load', $loadSY)
                                ->with('gradesData', $gradesData);
            }
        } else {
            return \Redirect::to('/login');
        }
    }

    public function getClassrecords($id) {
        if (\Auth::check()) {
            $user = \Auth::user()->userType;
            $term = 'Midterm';
            //$insID = \Input::get('instructorID');
            $recordData = $this->getRecordsData($id, $term);
            foreach ($recordData as $record) {
                $query = \DB::table('activities')->where('records_id', $record['id'])->get();
                $activities[] = json_decode(json_encode($query), true);
                $query1 = \DB::table('students')->where('id', $record['enrollment_students_id'])->get();
                $studentList[] = json_decode(json_encode($query1), true);
            }
            //dd($recordData);
            $classInfo = $this->getClassInfo($id);
            return view('pages.chairman.browse_classrecords')
                            ->with('id', $id)
                            ->with('term', $term)
                            ->with('user', $user)
                            ->with('classinfo', $classInfo)
                            ->with('activities', $activities)
                            ->with('records', $recordData)
                            ->with('studentlist', $studentList);
        } else {
            return \Redirect::to('/login');
        }
    }

    public function postClassrecords($id) {
        if (\Auth::check()) {
            $user = \Auth::user()->userType;
            $term = \Input::get('term');
            $recordData = $this->getRecordsData($id, $term);
            foreach ($recordData as $record) {
                $query = \DB::table('activities')->where('records_id', $record['id'])->get();
                $activities[] = json_decode(json_encode($query), true);
                $query1 = \DB::table('students')->where('id', $record['students_id'])->get();
                $studentList[] = json_decode(json_encode($query1), true);
            }

            $classInfo = $this->getClassInfo($id);
            // dd($activitiesData);
            return view('pages.chairman.browse_classrecords')
                            ->with('id', $id)
                            ->with('term', $term)
                            ->with('user', $user)
                            ->with('classinfo', $classInfo)
                            ->with('activities', $activities)
                            ->with('records', $recordData)
                            ->with('studentlist', $studentList);
        } else {
            return \Redirect::to('/login');
        }
    }

    public function postSavenewuser() {
        if (\Auth::check()) {
            $data = \Input::all();
            $value = "123";
            $encrypted = \Hash::make($value);
            \DB::insert('insert into users (id,username, name, gender,department,college,status,email,contactNumber,userType,password) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', array($data['idNumber'], $data['idNumber'], $data['sname'], $data['gender'], $data['department'], $data['college'], $data['status'], $data['email'], $data['contact'], $data['userType'], $encrypted));
            if ($data['userType'] == 'Instructor') {
                \DB::insert('insert into set_activities (name,type,users_id)
                     values (?,?,?)', array('Quiz', 'lec', $data['idNumber']));
                \DB::insert('insert into set_activities (name,type,users_id)
                     values (?,?,?)', array('TermExam', 'lec', $data['idNumber']));
                \DB::insert('insert into set_activities (name,type,users_id)
                     values (?,?,?)', array('TermExam-lab', 'lec', $data['idNumber']));
            }


            return \Redirect::back();
        }
    }

    public function postEditprofile($id) {
        $data = \Input::all();
        if (\Auth::check()) {
            if ($data['password'] === "") {
                \DB::table('users')->where('id', $id)->update(array('name' => $data['sname'],
                    'email' => $data['email'],
                    'gender' => $data['gender'],
                    'contactNumber' => $data['contact'],
                    'status' => $data['status'],
                    'username' => $data['username']));
            } else {
                $encryptedPassword = \Hash::make($data['password']);
                \DB::table('users')->where('id', $id)->update(array('name' => $data['sname'],
                    'email' => $data['email'],
                    'gender' => $data['gender'],
                    'contactNumber' => $data['contact'],
                    'status' => $data['status'],
                    'username' => $data['username'],
                    'password' => $encryptedPassword));
            }
        }
        return \Redirect::back();
    }

    public function postSavestudent() {
        $data = \Input::all();

        $count = 0;
        foreach ($data as $input) {
            $count++;
            $count1 = ($count - 6) / 3;
        }
        for ($i = 0; $i <= $count1; $i++) {
            \DB::insert('insert into students (id,sname,course,gender,year) values (?, ?, ?,?, ?)'
                    , array($data['sid' . $i], $data['sname' . $i], $data['course'],
                $data['gender' . $i], $data['year']));
        }
        return \Redirect::back();
    }

    public function postPresetup($id) {
        if (\Auth::check()) {
            $data = \Input::all();
            dd($id);
            $setActivities = $this->getSelectedActivities($id);
            $query = \DB::table('students')->get();
            $resultArray = json_decode(json_encode($query), true);
            dd($resultArray);
            $instructorLoad = $this->getInstructorLoad($id);
            \DB::insert('insert into faculty_load (subject, class, semester, subj_code, school_year_sy, school_year_users_id, units) values (?,?, ?, ?, ?, ?, ?)', array($data['subject'], $data['section'], $data['semester'], $data['subcode'], $data['sy'], $id, $data['units']));
            return \Redirect::back();
        } else {
            return \Redirect::to('/login');
        }
    }

    public function getAdmin() {
        if (\Auth::check()) {
            $user = \Auth::user()->userType;
            // $availableClasses = $this->getclasses();
            // if ($user == 'Admin') {
            /*
              $query = \DB::table('college')->get();
              $frmcollege = json_decode(json_encode($query), true);
              $query = \DB::table('department')->get();
              $frmdept = json_decode(json_encode($query), true);

              foreach ($frmdept as $key => $value) {

              }
              //dd($frmdept);
              foreach ($frmcollege as $colkey => $college) {
              foreach ($frmdept as $depkey => $dept) {

              }
              }
             */
            return View('pages.admin.browse_admin_file');
            // } else {
            //    die('Your not allowed to view this page');
            //  }
        } else {
            return \Redirect::to('/login');
        }
    }

    public function getDean() {
        if (\Auth::check()) {
            $user = \Auth::user()->userType;
            $college = \Auth::user()->college;
            $departmentData = $this->getDepartmentList($college);
            // dd($departmentData);
            if ($user == 'Dean') {
                return View('pages.dean.browse_dean_file')->with('departmentList', $departmentData);
            } else {
                die('Your not allowed to view this page');
            }
        } else {
            return \Redirect::to('/login');
        }
    }

    public function getEdituser() {
        if (\Auth::check()) {
            $user = \Auth::user()->userType;
            $college = "CEA";
            $instructorList = $this->getCollegeDepartment($college);
            if ($user == 'Admin') {
                return View('pages.admin.browse_users_file')->with('InstructorList', $instructorList);
            } else {
                die('Your not allowed to view this page');
            }
        } else {
            return \Redirect::to('/login');
        }
    }

    public function postEdituser() {
        if (\Auth::check()) {
            $user = \Auth::user()->userType;
            $college = \Input::get('college');
            $instructorList = $this->getCollegeDepartment($college);
            //$instructorList = $this->getCollegeDepartment($college);
            if ($user == 'Admin') {
                return View('pages.admin.browse_users_file')->with('InstructorList', $instructorList);
            } else {
                die('Your not allowed to view this page');
            }
        } else {
            return \Redirect::to('/login');
        }
    }

    public function getViewload($id) {
        if (\Auth::check()) {
            $user = \Auth::user()->userType;
            $instructorLoad = $this->getInstructorLoad($id);
            $instructorData = $this->generateInstructorData($id);
            //  dd($instructorData);
            if ($user == 'Admin') {
                return View('pages.admin.browse_loading')
                                ->with('instructorLoad', $instructorLoad)
                                ->with('id', $id)
                                ->with('instructorData', $instructorData);
            } else {
                die('Your not allowed to view this page');
            }
        } else {
            return \Redirect::to('/login');
        }
    }

    public function getViewinstructorload() {
        if (\Auth::check()) {
            $id = \Auth::user()->id;
            $user = \Auth::user()->userType;
            $instructorLoad = $this->getInstructorLoad($id);
            $instructorData = $this->generateInstructorData($id);
            //  dd($instructorData);

            if ($user == 'Instructor') {
                return View('pages.instructor.browse_viewload')
                                ->with('instructorLoad', $instructorLoad)
                                ->with('id', $id)
                                ->with('instructorData', $instructorData);
            } else {
                die('Your not allowed to view this page');
            }
        } else {
            return \Redirect::to('/login');
        }
    }

    public function postAddload($id) {
        if (\Auth::check()) {
            $data = \Input::all();
            // dd($data);
            $user = \Auth::user()->userType;
            $instructorLoad = $this->getInstructorLoad($id);
            //dd($instructorLoad);
            if ($user == 'Admin') {

                \DB::insert('insert into faculty_load (subject, class, semester, subj_code, school_year_sy, school_year_users_id, units) values (?,?, ?, ?, ?, ?, ?)', array($data['subject'], $data['section'], $data['semester'], $data['subcode'], $data['sy'], $id, $data['units']));
                $LoadId = $this->getLoadId($data['subcode'], $data['section']);

                \DB::insert('insert into selected_activities (load_id,act_name,percentage,formula,color,type,fontcolor)
                    values (?,?,?,?,?,?,?)', array($LoadId[0]['id'], 'TermExam', '50', '4s-5/t', '#ffffff', 'lec', '#000000'));

                \DB::insert('insert into selected_activities (load_id,act_name,percentage,formula,color,type,fontcolor)
                    values (?,?,?,?,?,?,?)', array($LoadId[0]['id'], 'Quiz', '50', '4s-5/t', '#ffffff', 'lec', '#000000'));
                \DB::insert('insert into selected_activities (load_id,act_name,percentage,formula,color,type,fontcolor)
                    values (?,?,?,?,?,?,?)', array($LoadId[0]['id'], 'TermExam-lab', '100', '4s-5/t', '#ffffff', 'lab', '#000000'));

                \DB::insert('insert into set_term (name,percentage,faculty_load_id)
                   values (?,?,?)', array('Midterm', '34', $LoadId[0]['id']));
                \DB::insert('insert into set_term (name,percentage,faculty_load_id)
                   values (?,?,?)', array('lab', '60', $LoadId[0]['id']));
                \DB::insert('insert into set_term (name,percentage,faculty_load_id)
                   values (?,?,?)', array('Pre-Final', '66', $LoadId[0]['id']));
                \DB::insert('insert into set_term (name,percentage,faculty_load_id)
                   values (?,?,?)', array('lec', '40', $LoadId[0]['id']));
                return \Redirect::back();
            } else {
                die('Your not allowed to view this page');
            }
        } else {
            return \Redirect::to('/login');
        }
    }

    public function postEditload($id) {
        if (\Auth::check()) {
            $action = \Input::get('action');
            $data = \Input::all();
            //  dd($data);
            if ($action == 'delete') {
                \DB::delete('delete from faculty_load where id =' . $id);
            } else if ($action == 'save') {
                \DB::table('faculty_load')->where('id', $id)->update(array('subject' => $data['subject'],
                    'class' => $data['section'],
                    'semester' => $data['semester'],
                    'subj_code' => $data['subcode'],
                    'school_year_sy' => $data['sy'],
                    'school_year_users_id' => $data['id'],
                    'units' => $data['units']));
// \DB::update('update faculty_load set subject = .'.$data['subject'].',class=.$data['section'].', semester, subj_code, school_year_sy, school_year_users_id, year, units where id =' . $id);
            }
        }
        return \Redirect::back();
    }

    public function getSchool($department) {
        if (\Auth::check()) {
            //     dd($department);
            $instructorData = $this->getInstructors($department);
            $chairmanData = $this->getChairmanName($department);
            //  dd($instructorData);
            return view('pages.dean.browse_instructors')->with('instructors', $instructorData)
                            ->with('chairman', $chairmanData);
        } else {
            return \Redirect::to('/login');
        }
    }

    //
    public function getRecords() {
        if (\Auth::check()) {
            $user = \Auth::user()->userType;
            if ($user == 'Instructor') {
                return View('pages.instructor.browse_student_file');
            } else {
                die('Your not allowed to view this page');
            }
        } else {
            return \Redirect::to('/')->withErrors('Login first to access the records.');
        }
    }

    public function getLogin() {
        if (\Auth::check()) {
            $user = \Auth::user()->userType;
            $department = \Auth::user()->department;
            $college = \Auth::user()->college;
            if ($user == 'Instructor') {
                return \Redirect::to('/instructor');
            }
            if ($user == 'Admin') {
                return \Redirect::to('/admin');
            }
            if ($user == 'Dean') {
                return \Redirect::to('/dean/' . $college);
            }
            if ($user == 'Chairman') {
                return \Redirect::to('/chairman/' . $department);
            }
        } else {
            return view('pages.login');
        }
    }

    public function postLogin(Request $request) {
        $this->validate($request, [
            'username' => 'required', 'password' => 'required',
        ]);

        $credentials = $request->only('username', 'password');

        if (\Auth::attempt($credentials, $request->has('remember'))) {
            return redirect()->intended($this->redirectPath());
        }

        return redirect($this->loginPath())
                        ->withInput($request->only('username', 'remember'))
                        ->withErrors([
                            'username' => $this->getFailedLoginMessage(),
        ]);
    }

    function postInstructor() {
        if (\Auth::check()) {
            $id = \Auth::user()->id;
            $schoolYear = \Input::get('schoolYear');
            $semester = \Input::get('semester');
            \Session::put('schoolYear', $schoolYear);
            \Session::put('semester', $semester);
            $query = \DB::table('students')->get();
            $studentData = json_decode(json_encode($query), true);
            $getSubject = $this->generateSubject($schoolYear, $semester, $id);
            $loadSY = $this->getLoadSY($id);
            $query = \DB::table('set_activities')->where('users_id', $id)->get();
            $set_list = json_decode(json_encode($query), true);
            //  dd($getSubject);
            return view('pages.instructor.browse_subjects')->with('load', $loadSY)
                            ->with('subjectData', $getSubject)
                            ->with('set_list', $set_list);
        } else {
            return \Redirect::to('/login');
        }
    }

    function getInstructor() {
        if (\Auth::check()) {
            $schoolYear = \Session::get('schoolYear');
            $semester = \Session::get('semester');
            $user = \Auth::user()->userType;


            // if ($user == 'Instructor') {
            $id = \Auth::user()->id;
            $getSubject = $this->generateSubject($schoolYear, $semester, $id);
            $loadSY = $this->getLoadSY($id);
            $query = \DB::table('set_activities')->where('users_id', $id)->get();
            $set_list = json_decode(json_encode($query), true);
            //dd($getSubject);
            return view('pages.instructor.browse_subjects')->with('load', $loadSY)
                            ->with('subjectData', $getSubject)
                            ->with('set_list', $set_list);
            //    } else {
            //        die('Your not allowed to view this page');
            //    }
        } else {
            return \Redirect::to('/login');
        }
    }

    function getClass($subject_id) {
        if (\Auth::check()) {
            $id = \Auth::user()->id;
            if (\Session::get('term') != 'Midterm' && \Session::get('term') != 'Pre-Final') {
                $term = 'Midterm';
                \Session::put('term', $term);
            } else {
                $term = \Session::get('term');
            }
            //  dd($term);
            \Session::put('subject_id', $subject_id);
            $SY = \Session::get('schoolYear');
            $sem = \Session::get('semester');

            $query = \DB::table('faculty_load')->where('id', $subject_id)->select('with_lab')->get();
            $wilab = json_decode(json_encode($query), true);
            $withlab = $wilab[0]['with_lab'];

            $count = \DB::table('selected_activities')->where('load_id', $subject_id)->where('type', 'lab')->count();

            if ($withlab == 1 && $count > 1) {   //// ----- WITH LAB STARTS --------////
                $activitiesData = $this->getActivitiesData($subject_id, $term);
                $activitiesDatalab = $this->getActivitiesDatalab($subject_id, $term);

                //dd($activitiesDatalab);

                $varss = array();
                $count = 0;
                foreach ($activitiesData as $data) {
                    $count++;
                    ;
                    foreach ($data as $data2) {
                        $varss[$data2['students_id']][preg_replace('/[0-9]+/', '', $data2['act_name'])]['score'][] = $data2['score'];
                        $varss[$data2['students_id']][preg_replace('/[0-9]+/', '', $data2['act_name'])]['total'][] = $data2['total'];
                    }
                    $varss[$data2['students_id']]['TermExam']['score'][] = $data2['exam_score'];
                    $varss[$data2['students_id']]['TermExam']['total'][] = $data2['exam_total'];
                    $recless[$data2['students_id']]['records_id'] = $data2['records_id'];
                }

                $varsslab = array();
                $count = 0;
                foreach ($activitiesDatalab as $data) {
                    $count++;
                    ;
                    foreach ($data as $data2) {
                        $varsslab[$data2['students_id']][preg_replace('/[0-9]+/', '', $data2['act_name'])]['score'][] = $data2['score'];
                        $varsslab[$data2['students_id']][preg_replace('/[0-9]+/', '', $data2['act_name'])]['total'][] = $data2['total'];
                    }
                    $varsslab[$data2['students_id']]['TermExam-lab']['score'][] = $data2['examlab_score'];
                    $varsslab[$data2['students_id']]['TermExam-lab']['total'][] = $data2['examlab_total'];
                    $reclesslab[$data2['students_id']]['records_id'] = $data2['records_id'];
                }

                //dd($varss);
                $gradeslist = array();
                $termgrade = array();
                $temp = array();
                $temp2 = array();
                $actgrade = 0;
                foreach ($varss as $datk => $data) {
                    $counter = 0;
                    foreach ($data as $dat2k => $data2) {

                        $indi = true;
                        foreach ($data2 as $data3) {
                            if ($indi) {
                                $count = 0;
                                foreach ($data3 as $data4) {
                                    $actgrade = $actgrade + ( 5 - ( ( 4 * $data2['score'][$count] ) / $data2['total'][$count]));
                                    $temp[$count] = $actgrade;
                                    $count++;
                                }
                                $indi = false;
                            }
                        }
                        $gradesupd[$datk][$dat2k]['Grade'] = $actgrade / $count;
                        $gradesupd[$datk][$dat2k]['Name'] = $dat2k;
                        $forma = number_format(($actgrade / $count), 1, '.', '');
                        $gradeslist[$datk][$dat2k]['Grade'] = $forma;
                        $gradeslist[$datk][$dat2k]['Name'] = $dat2k;
                        $ggra[$datk][$dat2k] = $gradeslist[$datk][$dat2k];
                        $actgrade = 0;
                        $counter++;
                    }
                }

                $gradeslistlab = array();
                $termgradelab = array();
                $temp = array();
                $temp2 = array();
                $actgrade = 0;
                foreach ($varsslab as $datk => $data) {
                    $counter = 0;
                    foreach ($data as $dat2k => $data2) {

                        $indi = true;
                        foreach ($data2 as $data3) {
                            if ($indi) {
                                $count = 0;
                                foreach ($data3 as $data4) {
                                    $actgrade = $actgrade + ( 5 - ( ( 4 * $data2['score'][$count] ) / $data2['total'][$count]));
                                    $temp[$count] = $actgrade;
                                    $count++;
                                }
                                $indi = false;
                            }
                        }
                        $gradesupdlab[$datk][$dat2k]['Grade'] = $actgrade / $count;
                        $gradesupdlab[$datk][$dat2k]['Name'] = $dat2k;
                        $forma = number_format(($actgrade / $count), 1, '.', '');
                        $gradeslistlab[$datk][$dat2k]['Grade'] = $forma;
                        $gradeslistlab[$datk][$dat2k]['Name'] = $dat2k;
                        $ggralab[$datk][$dat2k] = $gradeslistlab[$datk][$dat2k];
                        $actgrade = 0;
                        $counter++;
                    }
                }

                $actgrade = array();
                foreach ($varss as $datk => $data) {
                    foreach ($data as $dat2k => $data2) {
                        foreach ($data2 as $dat3k => $data3) {
                            foreach ($data3 as $dat4k => $data4) {
                                $query = \DB::table('records')->where('enrollment_students_id', $datk)->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->select('records.id as records_id')->get();
                                $recid = json_decode(json_encode($query), true);
                                \DB::table('grades')->where('records_id', $recid[0]['records_id'])->where('records_enrollment_students_id', $datk)->where('records_enrollment_faculty_load_id', $subject_id)->where('name', $dat2k)->update(array('grade' => (5 - (4 * $varss[$datk][$dat2k]['score'][$dat4k] / $varss[$datk][$dat2k]['total'][$dat4k]))));
                                $actgrade[$datk][$dat2k]['Grade'] = (5 - (4 * $varss[$datk][$dat2k]['score'][$dat4k] / $varss[$datk][$dat2k]['total'][$dat4k]));
                                $actgrade[$datk][$dat2k]['Name'] = $dat2k;
                            }
                        }
                    }
                }

                $actgradelab = array();
                foreach ($varsslab as $datk => $data) {
                    foreach ($data as $dat2k => $data2) {
                        foreach ($data2 as $dat3k => $data3) {
                            foreach ($data3 as $dat4k => $data4) {
                                $query = \DB::table('records')->where('enrollment_students_id', $datk)->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->select('records.id as records_id')->get();
                                $recid = json_decode(json_encode($query), true);
                                \DB::table('grades')->where('records_id', $recid[0]['records_id'])->where('records_enrollment_students_id', $datk)->where('records_enrollment_faculty_load_id', $subject_id)->where('name', $dat2k)->update(array('grade' => (5 - (4 * $varsslab[$datk][$dat2k]['score'][$dat4k] / $varsslab[$datk][$dat2k]['total'][$dat4k]))));
                                $actgradelab[$datk][$dat2k]['Grade'] = (5 - (4 * $varsslab[$datk][$dat2k]['score'][$dat4k] / $varsslab[$datk][$dat2k]['total'][$dat4k]));
                                $actgradelab[$datk][$dat2k]['Name'] = $dat2k;
                            }
                        }
                    }
                }

                $query = \DB::table('selected_activities')->where('type', 'lec')->where('load_id', $subject_id)->get();
                $seleact = json_decode(json_encode($query), true);

                $selarr = array();
                foreach ($seleact as $key => $data) {
                    $selarr[$data['act_name']] = $data;
                }

                $query = \DB::table('selected_activities')->where('type', 'lab')->where('load_id', $subject_id)->get();
                $seleactlab = json_decode(json_encode($query), true);

                $selarrlab = array();
                foreach ($seleactlab as $key => $data) {
                    $selarrlab[$data['act_name']] = $data;
                }

                $edarg = array();
                foreach ($ggra as $key => $data) {
                    $sum = 0;
                    foreach ($data as $key2 => $data2) {
                        $sum = $sum + ($data2['Grade'] * ($selarr[$key2]['percentage'] / 100));
                    }
                    $edarg[$key]['Grade'] = $sum;
                    $edarg[$key]['records_id'] = $recless[$key]['records_id'];
                }

                $edarglab = array();
                foreach ($ggralab as $key => $data) {
                    $sum = 0;
                    foreach ($data as $key2 => $data2) {
                        $sum = $sum + ($data2['Grade'] * ($selarrlab[$key2]['percentage'] / 100));
                    }
                    $edarglab[$key]['Grade'] = $sum;
                    $edarglab[$key]['records_id'] = $recless[$key]['records_id'];
                }

                $actcount = 0;
                foreach ($ggra as $data) {
                    $studcount = 0;
                    foreach ($data as $data2) {
                        $studcount++;
                    }
                    $actcount++;
                }

                $newgrade = array();
                foreach ($ggra as $key => $data) {
                    $count = 0;
                    foreach ($data as $key2 => $data2) {
                        $newgrade[$key][$count]['Name'] = $key2;
                        $newgrade[$key][$count]['Grade'] = $data2['Grade'];
                        $count++;
                    }
                }

                $actcount = 0;
                foreach ($ggralab as $data) {
                    $studcount = 0;
                    foreach ($data as $data2) {
                        $studcount++;
                    }
                    $actcount++;
                }

                $newgradelab = array();
                foreach ($ggralab as $key => $data) {
                    $count = 0;
                    foreach ($data as $key2 => $data2) {
                        $newgradelab[$key][$count]['Name'] = $key2;
                        $newgradelab[$key][$count]['Grade'] = $data2['Grade'];
                        $count++;
                    }
                }
                //dd($newgrade);
                //dd($gradeslist);
                foreach ($gradesupd as $datk => $data) {
                    foreach ($data as $data2) {
                        $query = \DB::table('records')->where('enrollment_students_id', $datk)->where('term', $term)->where('enrollment_faculty_load_id', $subject_id)->get();
                        $records_detail = json_decode(json_encode($query), true);
                        \DB::table('grades')->where('records_id', $records_detail[0]['id'])->where('records_enrollment_students_id', $datk)->where('records_enrollment_faculty_load_id', $subject_id)->where('name', $data2['Name'])->get();
                        $checker = json_decode(json_encode($query), true);
                        if ($data2['Name'] == 'TermExam') {
                            \DB::table('records')->where('enrollment_students_id', $datk)->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->update(array('exam_grade' => $data2['Grade']));
                        }
                        if ($checker != null) {
                            \DB::table('grades')->where('records_id', $records_detail[0]['id'])->where('records_enrollment_students_id', $datk)->where('records_enrollment_faculty_load_id', $subject_id)->where('name', $data2['Name'])->update(array('grade' => $data2['Grade']));
                        } else {
                            \DB::insert('insert into grades (records_id, records_enrollment_students_id, records_enrollment_faculty_load_id, records_enrollment_id, grade, name) values (?, ?, ?, ?, ?, ?)', array($records_detail[0]['id'], $datk, $subject_id, $records_detail[0]['enrollment_id'], $data2['Name'], $data2['Grade']));
                        }
                    }
                }

                foreach ($gradesupdlab as $datk => $data) {
                    foreach ($data as $data2) {
                        $query = \DB::table('records')->where('enrollment_students_id', $datk)->where('term', $term)->where('enrollment_faculty_load_id', $subject_id)->get();
                        $records_detail = json_decode(json_encode($query), true);
                        \DB::table('grades')->where('records_id', $records_detail[0]['id'])->where('records_enrollment_students_id', $datk)->where('records_enrollment_faculty_load_id', $subject_id)->where('name', $data2['Name'])->get();
                        $checker = json_decode(json_encode($query), true);

                        if ($data2['Name'] == 'TermExam-lab') {
                            \DB::table('records')->where('enrollment_students_id', $datk)->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->update(array('exam_grade' => $data2['Grade']));
                        }
                        if ($checker != null) {
                            \DB::table('grades')->where('records_id', $records_detail[0]['id'])->where('records_enrollment_students_id', $datk)->where('records_enrollment_faculty_load_id', $subject_id)->where('name', $data2['Name'])->update(array('grade' => $data2['Grade']));
                        } else {
                            \DB::insert('insert into grades (records_id, records_enrollment_students_id, records_enrollment_faculty_load_id, records_enrollment_id, grade, name) values (?, ?, ?, ?, ?, ?)', array($records_detail[0]['id'], $datk, $subject_id, $records_detail[0]['enrollment_id'], $data2['Grade'], $data2['Name']));
                        }
                    }
                }

                $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'Midterm')->select('percentage')->get();
                $dmidterm = json_decode(json_encode($query), true);

                if ($dmidterm == null) {
                    $dmidterm = 0;
                } else {
                    $dmidterm = $dmidterm[0]['percentage'];
                }
                $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'Pre-Final')->select('percentage')->get();
                $dpre = json_decode(json_encode($query), true);

                if ($dpre == null) {
                    $dpre = 0;
                } else {
                    $dpre = $dpre[0]['percentage'];
                }
                $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'lec')->select('percentage')->get();
                $dlec = json_decode(json_encode($query), true);

                if ($dlec == null) {
                    $dlec = 0;
                } else {
                    $dlec = $dlec[0]['percentage'];
                }
                $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'lab')->select('percentage')->get();
                $dlab = json_decode(json_encode($query), true);

                if ($dlab == null) {
                    $dlab = 0;
                } else {
                    $dlab = $dlab[0]['percentage'];
                }

                $query = \DB::table('set_activities')->where('users_id', $id)->where('type', 'lec')->select('set_activities.name')->get();
                $set_list = json_decode(json_encode($query), true);
                $act_list = array();
                //dd($set_list);
                foreach ($set_list as $list) {
                    $query = \DB::table('selected_activities')->where('load_id', $subject_id)->where('type', 'lec')->where('act_name', $list['name'])->get();
                    $acts_list = json_decode(json_encode($query), true);
                    $acts_det[] = json_decode(json_encode($query), true);
                    $act_detail = array_column($acts_det, 0);
                    if ($acts_list == null) {
                        array_push($act_list, $list['name']);
                    }
                }

                $query = \DB::table('set_activities')->where('users_id', $id)->where('type', 'lab')->select('set_activities.name')->get();
                $set_lablist = json_decode(json_encode($query), true);
                $act_lablist = array();
                //dd($set_list);
                foreach ($set_lablist as $list) {
                    $query = \DB::table('selected_activities')->where('load_id', $subject_id)->where('type', 'lab')->where('act_name', $list['name'])->get();
                    $acts_list = json_decode(json_encode($query), true);
                    $acts_det[] = json_decode(json_encode($query), true);
                    $act_detail = array_column($acts_det, 0);
                    if ($acts_list == null) {
                        array_push($act_lablist, $list['name']);
                    }
                }

                $lab_list = array();
                foreach ($set_list as $list) {
                    $query = \DB::table('selected_activities')->where('type', 'lab')->where('load_id', $subject_id)->where('act_name', $list['name'])->get();
                    $acts_list = json_decode(json_encode($query), true);
                    $acts_det[] = json_decode(json_encode($query), true);
                    $act_detail = array_column($acts_det, 0);
                    //dd($act_detail);
                    if ($acts_list == null) {
                        array_push($lab_list, $list['name']);
                    }
                }


                $temp_count = array();
                foreach ($activitiesData[0] as $data) {
                    $temp_count[preg_replace('/[0-9]+/', '', $data['act_name'])][] = $data['act_name'];
                }
                $act_count;
                foreach ($temp_count as $datk => $data) {
                    $count = 0;
                    foreach ($data as $data2) {
                        $count++;
                    }
                    $act_count[$datk] = $count;
                }

                foreach ($act_detail as $data) {
                    \DB::table('selected_activities')->where('load_id', $subject_id)->where('act_name', $data['act_name'])->update(array('count' => 0));
                }

                $actcount = array();
                foreach ($act_count as $datk => $data) {

                    $actcount[$datk]['name'] = $datk;
                    $actcount[$datk]['count'] = $data;
                    //dd($newname);
                }

                $query = \DB::table('records')->join('set_term', 'set_term.faculty_load_id', '=', 'records.enrollment_faculty_load_id')->where('records.enrollment_faculty_load_id', $subject_id)->get();
                $recset = json_decode(json_encode($query), true);

                $bforadd = array();
                foreach ($recset as $key => $data) {
                    if (( $data['term'] == 'Midterm' && $data['name'] == 'Midterm' ) || ( $data['term'] == 'Pre-Final' && $data['name'] == 'Pre-Final' )) {
                        $bforadd[$data['enrollment_students_id']]['final'][] = $data['term_grade'] * ($data['percentage'] / 100);
                        if ($data['term'] == 'Midterm') {
                            $bforadd[$data['enrollment_students_id']]['midterm_grade'] = $data['term_grade'];
                        } else if ($data['term'] == 'Pre-Final') {
                            $bforadd[$data['enrollment_students_id']]['prefinal_grade'] = $data['term_grade'];
                        }
                    }
                }

                $finalgrade = array();
                foreach ($bforadd as $key => $data) {
                    foreach ($data as $key2 => $data2) {
                        $finalgrade[$key] = array_sum($bforadd[$key]['final']);
                        \DB::table('overall')->where('enrollment_faculty_load_id', $subject_id)->where('enrollment_students_id', $key)->update(array('final_grade' => array_sum($bforadd[$key]['final']), 'midterm_grade' => $bforadd[$key]['midterm_grade'], 'prefinal_grade' => $bforadd[$key]['prefinal_grade'], 'remarks' => number_format(array_sum($bforadd[$key]['final']), 1, '.', '') <= 3 ? 'Passed' : 'Failed'));
                    }
                }
                $gradesData = $this->getGradesData($subject_id);

                foreach ($activitiesDatalab as $key => $data) {
                    foreach ($data as $key2 => $data2) {
                        $activitiesData[$key][] = $data2;
                    }
                }

                foreach ($newgradelab as $key => $data) {
                    foreach ($data as $key2 => $data2) {
                        $newgrade[$key][] = $data2;
                    }
                }

                $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->get();
                $termpe = json_decode(json_encode($query), true);

                foreach ($termpe as $key => $data) {
                    $termperc[$data['name']] = $data;
                }

                foreach ($edarglab as $key => $data) {
                    foreach ($data as $key2 => $data2) {
                        $newedarg[$key]['Grade'] = number_format(($edarg[$key]['Grade'] * $termperc['lec']['percentage'] / 100) + ($data['Grade'] * $termperc['lab']['percentage'] / 100), 1, '.', '');
                        $newedarg[$key]['records_id'] = $data['records_id'];
                        \DB::table('records')->where('enrollment_students_id', $key)->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->update(array('term_grade' => (($edarg[$key]['Grade'] * $termperc['lec']['percentage'] / 100) + ($data['Grade'] * $termperc['lab']['percentage'] / 100))));
                    }
                }
                $edarg = $newedarg;
            } else { /// ----- WITHOUT LAB STARTS -------- ///
                $activitiesData = $this->getActivitiesData($subject_id, $term);

                $varss = array();
                $count = 0;
                foreach ($activitiesData as $data) {
                    $count++;
                    ;
                    foreach ($data as $data2) {
                        $varss[$data2['students_id']][preg_replace('/[0-9]+/', '', $data2['act_name'])]['score'][] = $data2['score'];
                        $varss[$data2['students_id']][preg_replace('/[0-9]+/', '', $data2['act_name'])]['total'][] = $data2['total'];
                    }
                    $varss[$data2['students_id']]['TermExam']['score'][] = $data2['exam_score'];
                    $varss[$data2['students_id']]['TermExam']['total'][] = $data2['exam_total'];
                    $recless[$data2['students_id']]['records_id'] = $data2['records_id'];
                }
                //dd($varss);
                $gradeslist = array();
                $termgrade = array();
                $temp = array();
                $temp2 = array();
                $actgrade = 0;
                foreach ($varss as $datk => $data) {
                    $counter = 0;
                    foreach ($data as $dat2k => $data2) {

                        $indi = true;
                        foreach ($data2 as $data3) {
                            if ($indi) {
                                $count = 0;
                                foreach ($data3 as $data4) {
                                    $actgrade = $actgrade + ( 5 - ( ( 4 * $data2['score'][$count] ) / $data2['total'][$count]));
                                    $temp[$count] = $actgrade;
                                    $count++;
                                }
                                $indi = false;
                            }
                        }
                        $gradesupd[$datk][$dat2k]['Grade'] = $actgrade / $count;
                        $gradesupd[$datk][$dat2k]['Name'] = $dat2k;
                        $forma = number_format(($actgrade / $count), 1, '.', '');
                        $gradeslist[$datk][$dat2k]['Grade'] = $forma;
                        $gradeslist[$datk][$dat2k]['Name'] = $dat2k;
                        $ggra[$datk][$dat2k] = $gradeslist[$datk][$dat2k];
                        $actgrade = 0;
                        $counter++;
                    }
                }
                $actgrade = array();
                foreach ($varss as $datk => $data) {
                    foreach ($data as $dat2k => $data2) {
                        foreach ($data2 as $dat3k => $data3) {
                            foreach ($data3 as $dat4k => $data4) {
                                $query = \DB::table('records')->where('enrollment_students_id', $datk)->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->select('records.id as records_id')->get();
                                $recid = json_decode(json_encode($query), true);
                                \DB::table('grades')->where('records_id', $recid[0]['records_id'])->where('records_enrollment_students_id', $datk)->where('records_enrollment_faculty_load_id', $subject_id)->where('name', $dat2k)->update(array('grade' => (5 - (4 * $varss[$datk][$dat2k]['score'][$dat4k] / $varss[$datk][$dat2k]['total'][$dat4k]))));
                                $actgrade[$datk][$dat2k]['Grade'] = (5 - (4 * $varss[$datk][$dat2k]['score'][$dat4k] / $varss[$datk][$dat2k]['total'][$dat4k]));
                                $actgrade[$datk][$dat2k]['Name'] = $dat2k;
                            }
                        }
                    }
                }

                $query = \DB::table('records')->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->get();
                $rec_det = json_decode(json_encode($query), true);
                //dd($rec_det);

                $query = \DB::table('selected_activities')->where('load_id', $subject_id)->get();
                $seleact = json_decode(json_encode($query), true);

                $selarr = array();
                foreach ($seleact as $key => $data) {
                    $selarr[$data['act_name']] = $data;
                }

                $edarg = array();
                foreach ($ggra as $key => $data) {
                    $sum = 0;
                    foreach ($data as $key2 => $data2) {
                        $sum = $sum + ($data2['Grade'] * ($selarr[$key2]['percentage'] / 100));
                    }
                    $edarg[$key]['Grade'] = number_format($sum, 1, '.', '');
                    $edarg[$key]['records_id'] = $recless[$key]['records_id'];
                    \DB::table('records')->where('enrollment_students_id', $key)->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->update(array('term_grade' => $sum));
                }

                $actcount = 0;
                foreach ($ggra as $data) {
                    $studcount = 0;
                    foreach ($data as $data2) {
                        $studcount++;
                    }
                    $actcount++;
                }

                $newgrade = array();
                foreach ($ggra as $key => $data) {
                    $count = 0;
                    foreach ($data as $key2 => $data2) {
                        $newgrade[$key][$count]['Name'] = $key2;
                        $newgrade[$key][$count]['Grade'] = $data2['Grade'];
                        $count++;
                    }
                }
                //dd($newgrade);
                //dd($gradeslist);
                foreach ($gradesupd as $datk => $data) {
                    foreach ($data as $data2) {
                        $query = \DB::table('records')->where('enrollment_students_id', $datk)->where('term', $term)->where('enrollment_faculty_load_id', $subject_id)->get();
                        $records_detail = json_decode(json_encode($query), true);
                        \DB::table('grades')->where('records_id', $records_detail[0]['id'])->where('records_enrollment_students_id', $datk)->where('records_enrollment_faculty_load_id', $subject_id)->where('name', $data2['Name'])->get();
                        $checker = json_decode(json_encode($query), true);
                        if ($data2['Name'] == 'TermExam') {
                            \DB::table('records')->where('enrollment_students_id', $datk)->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->update(array('exam_grade' => $data2['Grade']));
                        }
                        if ($checker != null) {
                            \DB::table('grades')->where('records_id', $records_detail[0]['id'])->where('records_enrollment_students_id', $datk)->where('records_enrollment_faculty_load_id', $subject_id)->where('name', $data2['Name'])->update(array('grade' => $data2['Grade']));
                        } else {
                            \DB::insert('insert into grades (records_id, records_enrollment_students_id, records_enrollment_faculty_load_id, grade, name) values (?, ?, ?, ?, ?)', array($records_detail[0]['id'], $datk, $subject_id, $data2['Name'], $data2['Grade']));
                        }
                    }
                }

                $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'Midterm')->select('percentage')->get();
                $dmidterm = json_decode(json_encode($query), true);

                if ($dmidterm == null) {
                    $dmidterm = 0;
                } else {
                    $dmidterm = $dmidterm[0]['percentage'];
                }
                $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'Pre-Final')->select('percentage')->get();
                $dpre = json_decode(json_encode($query), true);

                if ($dpre == null) {
                    $dpre = 0;
                } else {
                    $dpre = $dpre[0]['percentage'];
                }
                $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'lec')->select('percentage')->get();
                $dlec = json_decode(json_encode($query), true);

                if ($dlec == null) {
                    $dlec = 0;
                } else {
                    $dlec = $dlec[0]['percentage'];
                }
                $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'lab')->select('percentage')->get();
                $dlab = json_decode(json_encode($query), true);

                if ($dlab == null) {
                    $dlab = 0;
                } else {
                    $dlab = $dlab[0]['percentage'];
                }

                $query = \DB::table('set_activities')->where('users_id', $id)->where('type', 'lec')->select('set_activities.name')->get();
                $set_list = json_decode(json_encode($query), true);
                $act_list = array();
                //dd($set_list);
                foreach ($set_list as $list) {
                    $query = \DB::table('selected_activities')->where('load_id', $subject_id)->where('type', 'lec')->where('act_name', $list['name'])->get();
                    $acts_list = json_decode(json_encode($query), true);
                    $acts_det[] = json_decode(json_encode($query), true);
                    $act_detail = array_column($acts_det, 0);
                    if ($acts_list == null) {
                        array_push($act_list, $list['name']);
                    }
                }

                $query = \DB::table('set_activities')->where('users_id', $id)->where('type', 'lab')->select('set_activities.name')->get();
                $set_lablist = json_decode(json_encode($query), true);
                $act_lablist = array();
                //dd($set_list);
                foreach ($set_lablist as $list) {
                    $query = \DB::table('selected_activities')->where('load_id', $subject_id)->where('type', 'lab')->where('act_name', $list['name'])->get();
                    $acts_list = json_decode(json_encode($query), true);
                    $acts_det[] = json_decode(json_encode($query), true);
                    $act_detail = array_column($acts_det, 0);
                    if ($acts_list == null) {
                        array_push($act_lablist, $list['name']);
                    }
                }

                $lab_list = array();
                foreach ($set_list as $list) {
                    $query = \DB::table('selected_activities')->where('type', 'lab')->where('load_id', $subject_id)->where('act_name', $list['name'])->get();
                    $acts_list = json_decode(json_encode($query), true);
                    $acts_det[] = json_decode(json_encode($query), true);
                    $act_detail = array_column($acts_det, 0);
                    //dd($act_detail);
                    if ($acts_list == null) {
                        array_push($lab_list, $list['name']);
                    }
                }


                $temp_count = array();
                foreach ($activitiesData[0] as $data) {
                    $temp_count[preg_replace('/[0-9]+/', '', $data['act_name'])][] = $data['act_name'];
                }
                $act_count;
                foreach ($temp_count as $datk => $data) {
                    $count = 0;
                    foreach ($data as $data2) {
                        $count++;
                    }
                    $act_count[$datk] = $count;
                }

                foreach ($act_detail as $data) {
                    \DB::table('selected_activities')->where('load_id', $subject_id)->where('act_name', $data['act_name'])->update(array('count' => 0));
                }

                $actcount = array();
                foreach ($act_count as $datk => $data) {

                    $actcount[$datk]['name'] = $datk;
                    $actcount[$datk]['count'] = $data;
                    //dd($newname);
                }

                $query = \DB::table('records')->join('set_term', 'set_term.faculty_load_id', '=', 'records.enrollment_faculty_load_id')->where('records.enrollment_faculty_load_id', $subject_id)->get();
                $recset = json_decode(json_encode($query), true);

                $bforadd = array();
                foreach ($recset as $key => $data) {
                    if (( $data['term'] == 'Midterm' && $data['name'] == 'Midterm' ) || ( $data['term'] == 'Pre-Final' && $data['name'] == 'Pre-Final' )) {
                        $bforadd[$data['enrollment_students_id']]['final'][] = $data['term_grade'] * ($data['percentage'] / 100);
                        if ($data['term'] == 'Midterm') {
                            $bforadd[$data['enrollment_students_id']]['midterm_grade'] = $data['term_grade'];
                        } else if ($data['term'] == 'Pre-Final') {
                            $bforadd[$data['enrollment_students_id']]['prefinal_grade'] = $data['term_grade'];
                        }
                    }
                }

                $finalgrade = array();
                foreach ($bforadd as $key => $data) {
                    foreach ($data as $key2 => $data2) {
                        $finalgrade[$key] = array_sum($bforadd[$key]['final']);
                        \DB::table('overall')->where('enrollment_faculty_load_id', $subject_id)->where('enrollment_students_id', $key)->update(array('final_grade' => array_sum($bforadd[$key]['final']), 'midterm_grade' => $bforadd[$key]['midterm_grade'], 'prefinal_grade' => $bforadd[$key]['prefinal_grade'], 'remarks' => number_format(array_sum($bforadd[$key]['final']), 1, '.', '') <= 3 ? 'Passed' : 'Failed'));
                    }
                }
                $gradesData = $this->getGradesData($subject_id);
            }

            $query = \DB::table('faculty_load')->where('id', $subject_id)->select('with_lab')->get();
            $witab = json_decode(json_encode($query), true);
            $withlab = $witab[0]['with_lab'];
            return view('pages.instructor.browse_student_file')
                            ->with('term', $term)
                            ->with('activitiesData', $activitiesData)
                            ->with('subject_id', $subject_id)
                            ->with('SY', $SY)->with('sem', $sem)
                            ->with('act_list', $act_list)
                            ->with('act_detail', $act_detail)
                            ->with('gradeslist', $newgrade)
                            ->with('dmidterm', $dmidterm)
                            ->with('dpre', $dpre)
                            ->with('dlec', $dlec)
                            ->with('dlab', $dlab)
                            ->with('termgrade', $edarg)
                            ->with('act_lablist', $act_lablist)
                            ->with('gradesData', $gradesData)
                            ->with('withlab', $withlab);
        } else {            //$activitiesName = $this->getActivitiesList($subject_id);
            return \Redirect::to('/')->withErrors('Login first to view the subjects.');
        }
    }

    function postClass() {
        if (\Auth::check()) {
            $id = \Auth::user()->id;
            $term = \Input::get('term');
            if ($term == null) {
                $term = \Session::get('term');
            }
            //dd($term);
            \Session::put('term', $term);
            $subject_id = \Session::get('subject_id');
            return \Redirect::to('class/' . $subject_id);
        } else {
            return \Redirect::to('/')->withErrors('Login first to view the subjects.');
        }
    }

    function postUpdate() {
        $subject_id = \Session::get('subject_id');
        if (\Auth::check()) {
            $activitiesNo = \Input::get('activitiesNo');
            $recordsNo = \Input::get('recordsNo');
            for ($i = 1; $i < $activitiesNo; $i++) {
                $activities_id = \Input::get("activities" . $i);
                $score = \Input::get($activities_id);
                $actname = \Input::get('actname' . $i);
                $total = \Input::get($actname);
                \DB::table('activities')->where('id', $activities_id)->update(array('score' => $score, 'total' => $total));
            }

            for ($i = 1; $i < $recordsNo; $i++) {
                $records_id = \Input::get("records" . $i);
                $score = \Input::get($records_id . 'score');
                if ($i == 1) {
                    $total = \Input::get($records_id . 'total');
                    //dd($records_id);
                }
                \DB::table('records')->where('id', $records_id)->update(array('exam_score' => $score, 'exam_total' => $total));
            }
            return \Redirect::to('class/' . $subject_id);
        }
    }

    function postAddactivity() {
        $term = \Session::get('term');
        $subject_id = \Session::get('subject_id');
        $selected_act = \Input::get('selected_act');
        $total = \Input::get('total');

        $query = \DB::table('selected_activities')->where('act_name', $selected_act)->where('load_id', $subject_id)->select('type')->get();
        $selt = json_decode(json_encode($query), true);
        $sel_type = $selt[0]['type'];

        if ($selected_act != "Select Activity") {
            if ($total > 0 || is_numeric($total)) {
                //dd($total);
                $score = 0;
                $query = \DB::table('records')->where('enrollment_faculty_load_id', $subject_id)->where('term', $term)->select('records.id as id')->get();
                $records = json_decode(json_encode($query), true);

                $query = \DB::table('activities')->where('records_id', $records[0]['id'])->where('type', $sel_type)->where('act_name', 'LIKE', $selected_act . '%')->count();
                $counter = json_decode(json_encode($query), true);
                if ($counter != null) {
                    $selected = $selected_act . ($counter + 1);
                } else {
                    $selected = $selected_act . 1;
                }

                foreach ($records as $records) {
                    \DB::insert('insert into activities (act_name, score, total, records_id, type) values (?, ?, ?, ?, ?)', array($selected, $score, $total, $records['id'], $sel_type));
                }
            }
        }

        return \Redirect::back();
    }

    public function postSetup() {
        $id = \Auth::user()->id;
        $subject_id = \Session::get('subject_id');
        $inmidterm = \Input::get('Midterm');
        $inpre = \Input::get('Pre');
        $inlec = \Input::get('lec');
        $inlab = \Input::get('lab');
        $withlab = \Input::get('checklab');

        $query = \DB::table('set_activities')->where('users_id', $id)->select('set_activities.name')->where('type', 'lec')->get();
        $set_list = json_decode(json_encode($query), true);

        $act_list = array();
        foreach ($set_list as $list) {
            $query = \DB::table('selected_activities')->where('load_id', $subject_id)->where('type', 'lec')->where('act_name', $list['name'])->get();
            $acts_list = json_decode(json_encode($query), true);
            $acts_det[] = json_decode(json_encode($query), true);
            $act_detail = array_column($acts_det, 0);
            if ($acts_list == null) {
                array_push($act_list, $list['name']);
            }
            //
        }

        $query = \DB::table('set_activities')->where('users_id', $id)->where('type', 'lab')->select('set_activities.name')->get();
        $set_list = json_decode(json_encode($query), true);

        $act_lablist = array();
        foreach ($set_list as $list) {
            $query = \DB::table('selected_activities')->where('load_id', $subject_id)->where('type', 'lab')->where('act_name', $list['name'])->get();
            $acts_list = json_decode(json_encode($query), true);
            $acts_det[] = json_decode(json_encode($query), true);
            $act_detail = array_column($acts_det, 0);
            if ($acts_list == null) {
                array_push($act_lablist, $list['name']);
            }
        }

        foreach ($act_list as $data) {
            try {
                $act_name = $data;
                $percentage = \Input::get($act_name);
                //dd($act_name);
                $formula = "(5-(4S))/T";
                $color = \Input::get($act_name . 'color');
                $fontcolor = \Input::get($act_name . 'fontcolor');
                $count = 0;
                $type = 'lec';
                //dd($act_name);
                if ($percentage != null && $color != null) {
                    \DB::insert('insert into selected_activities (load_id, act_name, percentage, formula, color, fontcolor, type) values (?, ?, ?, ?, ?, ?, ?)', array($subject_id, $act_name, $percentage, $formula, $color, $fontcolor, $type));
                }
            } catch (Exception $e) {
                
            }
        }
        foreach ($act_detail as $data) {
            try {
                $act_name = $data['act_name'];
                $percentage = \Input::get($act_name);
                $formula = "(5-(4S))/T";
                $color = \Input::get($act_name . 'color');
                $fontcolor = \Input::get($act_name . 'fontcolor');
                \DB::table('selected_activities')->where('type', 'lec')->where('load_id', $subject_id)->where('act_name', $act_name)->update(array('act_name' => $act_name, 'percentage' => $percentage, 'formula' => $formula, 'color' => $color, 'fontcolor' => $fontcolor));
            } catch (Exception $e) {
                
            }
        }
        //dd($withlab);
        if ($withlab == "on") {
            \DB::table('faculty_load')->where('id', $subject_id)->update(array('with_lab' => 1));
            foreach ($act_lablist as $data) {
                try {
                    $act_name = $data;

                    $percentage = \Input::get($act_name . 'lab');

                    //dd($act_name);
                    $formula = "(5-(4S))/T";
                    $color = \Input::get($act_name . 'labcolor');
                    $fontcolor = \Input::get($act_name . 'labfontcolor');
                    $count = 0;
                    $type = 'lab';

                    if ($percentage != null && $color != null) {
                        \DB::insert('insert into selected_activities (load_id, act_name, percentage, formula, color, fontcolor, count, type) values (?, ?, ?, ?, ?, ?, ?, ?)', array($subject_id, $act_name, $percentage, $formula, $color, $fontcolor, $count, $type));
                    }
                } catch (Exception $e) {
                    
                }
            }
            foreach ($act_detail as $data) {
                try {
                    $act_name = $data['act_name'];
                    $percentage = \Input::get($act_name . 'lab');
                    $formula = "(5-(4S))/T";
                    $color = \Input::get($act_name . 'labcolor');
                    $fontcolor = \Input::get($act_name . 'labfontcolor');
                    \DB::table('selected_activities')->where('type', 'lab')->where('load_id', $subject_id)->where('act_name', $act_name)->update(array('act_name' => $act_name, 'percentage' => $percentage, 'formula' => $formula, 'color' => $color, 'fontcolor' => $fontcolor));
                } catch (Exception $e) {
                    
                }
            }
        } else {
            \DB::table('faculty_load')->where('id', $subject_id)->update(array('with_lab' => 0));
        }

        $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'Midterm')->get();
        $Midterm = json_decode(json_encode($query), true);
        $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'Pre-Final')->get();
        $Pre = json_decode(json_encode($query), true);
        $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'lec')->get();
        $lec = json_decode(json_encode($query), true);
        $query = \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'lab')->get();
        $lab = json_decode(json_encode($query), true);

        if ($Midterm == null) {
            \DB::insert('insert into set_term (faculty_load_id, name, percentage) values (?, ?, ?)', array($subject_id, 'Midterm', $inmidterm));
        } else {
            \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'Midterm')->update(array('percentage' => $inmidterm));
        }

        if ($Pre == null) {
            \DB::insert('insert into set_term (faculty_load_id, name, percentage) values (?, ?, ?)', array($subject_id, 'Pre-Final', $inpre));
        } else {
            \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'Pre-Final')->update(array('percentage' => $inpre));
        }

        if ($lec == null) {
            \DB::insert('insert into set_term (faculty_load_id, name, percentage) values (?, ?, ?)', array($subject_id, 'lec', $inlec));
        } else {
            \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'lec')->update(array('percentage' => $inlec));
        }

        if ($lab == null) {
            \DB::insert('insert into set_term (faculty_load_id, name, percentage) values (?, ?, ?)', array($subject_id, 'lab', $inlab));
        } else {
            \DB::table('set_term')->where('faculty_load_id', $subject_id)->where('name', 'lab')->update(array('percentage' => $inlab));
        }
        return \Redirect::to('class/' . $subject_id);
    }

    public function postSetfav() {
        $id = \Auth::user()->id;
        $actNo = \Input::get('actNo');

        for ($i = 1; $i <= $actNo; $i++) {
            $act_name = \Input::get('act' . $i);
            $act_type = \Input::get('act_type' . $i);
            if ($act_type == 'lecture') {
                $act_type = 'lec';
            } else {
                $act_type = 'lab';
            }
            $query = \DB::table('set_activities')->where('users_id', $id)->where('name', $act_name)->where('type', $act_type)->get();
            $checker = json_decode(json_encode($query), true);
            if ($checker == null) {
                \DB::insert('insert into set_activities (users_id, name, type) values (?, ?, ?)', array($id, $act_name, $act_type));
            }
        }
        return \Redirect::to('subjects');
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage() {
        return 'Incorrect username or password.';
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout() {
        \Auth::logout();
        \Session::flush();
        return redirect('/login');
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath() {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        $user = \Auth::user()->userType;
        $college = \Auth::user()->college;
        $department = \Auth::user()->department;
        if ($user == 'Instructor') {
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/instructor';
        }
        if ($user == 'Admin') {
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/admin';
        }
        if ($user == 'Dean') {
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/dean/' . $college;
        }
        if ($user == 'Chairman') {
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/chairman/' . $department;
        }
    }

    /**
     * Get the path to the login route.
     *
     * @return string
     */
    public function loginPath() {
        return property_exists($this, 'loginPath') ? $this->loginPath : '/login';
    }

}
