<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link rel="shortcut icon" href="/favicon.ico" />
<link href="./assets/css/bootstrap.min.css" rel="stylesheet"/>
<link href="./assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="./assets/css/style-responsive.css" rel="stylesheet"/>
<link href="./assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
<script src="./assets/js/jquery-1.11.2.min.js"></script>
<script src="./assets/js/bootstrap.min.js"></script>
<script src="./assets/js/bootstrap-select.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link href="./assets/css/StyleSheet.css" type="text/css" rel="stylesheet"> </link> 
    </head>
    <body class="master_style">
        <a href="browse_chairman_file.blade.php"></a>
        <div class="page_style">
            <img id="Image1" src="./assets/img/banner.png" style="width:100%;"> </img>
            <div class="styleling">

                <a href="pages/layout/topbar.blade.php"></a>
                <TITLE>Class Record</TITLe>
                <div class="pull-left">
                    <span  style="font-size:12pt;font-weight:bold;font-style:normal;" >Logged as </span> <span style="color:Blue;font-family:Verdana;font-size:12pt;font-weight:bold;font-style:normal;">{{ Auth::user()->id }}</span>
                    </br> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;  <span class="color1">[ADMIN]</span>
                </div>
                <div class="pull-right">                
                    <a style ="font-size:medium;" href = "./admin"> <span class = "glyphicon glyphicon-home"></span> Home</a> | 
                    <a style ="font-size:medium;"data-toggle = "modal" href = "#myModal10"> <span class = "glyphicon glyphicon-book"></span> Edit Profile</a> |  | 
                    <a style ="font-size:medium;" href="{{url('/logout')}}"><span class = "glyphicon glyphicon-off"></span> Logout</a>
                </div>
                <br>

                </br>
                </br>
                <table width="100%">
                    <tbody>

                        <td valign="middle" style="text-align: center; vertical-align:middle; padding:3px">
                            @if(Auth::user()->uploadedpic != 0)
                                <img  src="./assets/img/{{Auth::user()->id}}.jpg" style="height:65px;width:75px;margin-top: -4px">
                            @else
                                <img  src="./assets/img/profile.png" style="height:65px;width:75px;margin-top: -4px">
                            @endif
                        </td>
                        <td valign="top" style="width: 100%">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 100px;" class="TableHeader">
                                                            <span>Employee No.</span>
                                                        </td>
                                                        <td class="TableHeader">
                                                            <span>Name</span>
                                                        </td>
                                                        <td style="width: 75px;" class="TableHeader">
                                                            <span>Gender</span>
                                                        </td>
                                                        <td class="TableHeader">
                                                            <span>Status</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px; height: 30px; text-align: center;">
                                                            <span style="font-size:Larger;font-weight:normal;">{{ Auth::user()->id }}</span></td>
                                                        <td style="text-align: center">
                                                            <span  style="font-size:Larger;font-weight:normal;">{{ Auth::user()->name }}</span></td>
                                                        <td style="width: 75px; text-align: center;">
                                                            <span style="font-size:Larger;font-weight:normal;">{{ Auth::user()->gender }}</span></td>
                                                        <td style="text-align: center">
                                                            <span  style="font-size:Larger;font-weight:normal;">{{ Auth::user()->status }}</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: gainsboro" valign="bottom">
                                            <table style="font-size: smaller; width: 100%; margin-left: 10px">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <span>Email : </span>
                                                            <span>{{ Auth::user()->email }}</span>
                                                        </td>
                                                        <td>
                                                            <span>Mobile No. :</span>
                                                            <span>{{ Auth::user()->contactNumber }}</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </td>
                    </tbody>
                </table>
                @include('pages.admin.users_file')
                <br />
                <br />


                @include('pages.change_profile')

                <img id="Image3" src="./assets/img/wave2.jpg" style="width:100%;"> </img>
            </div>
            <img id="Image3" src="./assets/img/wave.jpg" style="width:100%;"> </img>
            <img id="Image2" src="./assets/img/cmubanner2.png" style="width:100%;"></img>
        </div>

    </body>
</html>