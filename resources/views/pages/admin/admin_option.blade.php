
<br>
<br>
<div class = "container" style = "padding-left:25%;">
    <div  class = "row col-lg-12">
        <div class="col-lg-3 col-md-3 col-xs-3">
            <a data-toggle ="modal" href = "#myModal1">
                <img src = "./assets/img/addStudent.png" style = "width:100px;height: 100px;"/>
                <br>
                <label style = "font-size: medium;">Add Student</label>
            </a>  

        </div>
        <div class="col-lg-3 col-md-3 col-xs-3">

            <a data-toggle ="modal" href = "#myModal2">
                <img src = "./assets/img/addUser.png" style = "width:100px;height: 100px;"/>
                <br>
                <label style = "font-size: medium;padding-left: 5%;">Add User</label>
            </a>  

        </div>
        <div class="col-lg-3 col-md-3 col-xs-3">

            <a data-toggle ="modal" href = "./edituser">
                <img src = "./assets/img/ece.png" style = "width:100px;height: 100px;"/>
                <br>
                <label  style = "font-size: medium;padding-left: 5%;">Faculty</label>
            </a>  
        </div>


    </div>
</div>
<!-- Modal -->


<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
        <form method ="post" role="form" action = "./savenewuser" id ="form2">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <div class="modal-content">
                <div class="modal-header">
                    <span class="glyphicon glyphicon-plus "></span>
                </div>

                <div class="modal-body">

                    <div class = "container col-lg-12">

                        @include('pages.admin.main_add')
                    </div>
                    <div class = "container col-lg-12">
                        <div id = "instructor" class ="form-group">
                            <div class="form-group col-lg-5 col-md-5 col-xs-5">
                                <label for="department">Department</label>
                                <select  placeholder="department" class="form-control" name="department">
                                <option>Archi</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="status">Status</label>
                                <select  name = "status" class="form-control" size = "1">
                                    <option>Full-Time</option>
                                    <option>Part-Time</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-xs-4">
                                <label for="college1">College</label>
                                <input type="text"  placeholder="college" class="form-control" name="college">
                            </div>
                        </div>
                    </div>
                    <div class = "modal-footer">
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-file"></span>  Save</button>
                    </div>
                    </form>
                </div>
            </div>
    </div>
</div>
@include('pages.admin.add_student')
<script>
    var count = 0;
    function add() {
        count++;
        $('<div class="form-group col-lg-5"><input type="text"  placeholder="sname" class="form-control" name="sname' + count + '"/> </div> <div class="form-group col-lg-4"><input type="text"  placeholder="student Id" class="form-control" name="sid' + count + '"/> </div> <div class="form-group col-lg-3"> <select class = "form-control" name="gender' + count + '"><option>Male</option> <option>Female</option>  </select> </div>').appendTo("#parentDIV");
    }

</script>