
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form method ="post" role="form" action = "./savestudent" id ="form2">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><span class="glyphicon glyphicon-plus "></span> Add Student</h4>
                </div>


                <div class="modal-body" style = "padding:0px 2px 0px 2px;">

                    <div class="panel-body col-lg-12">
                        <div class="form-group col-lg-5">
                            <label for="sname">Course</label>
                            <input type="text"  placeholder="course" class="form-control" name="course">
                        </div>
                        <div class="form-group col-lg-4 col-md-1 col-xs-1">
                            <label for="year">Year</label>
                            <select  name = "year" class="form-control" size = "1">
                                <option>1st</option>
                                <option>2nd</option>
                                <option>3rd</option>
                                <option>4th</option>
                                <option>5th</option>
                            </select>
                        </div>
                    </div>

                    <div id = "parentDIV" class="panel-body success col-lg-12">
                        <div class="form-group col-lg-5">
                            <label for="sname">Student Name</label>
                            <input type="text"  placeholder="sname" class="form-control" name="sname0">
                        </div>
                        <div class="form-group col-lg-4">
                            <label for="sname">Student ID</label>
                            <input type="text"  placeholder="student Id" class="form-control" name="sid0">
                        </div>
                        <div class="form-group col-lg-3 col-md-1 col-xs-1">
                            <label for="year">Gender</label>
                            <select  name = "gender0" class="form-control" size = "1">
                                <option>Male</option>
                                <option>Female</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-4 col-md-1 col-xs-1">
                        <a href="#"> <label class ="label label-primary"style = "font-size:small;" id = "addfield" onclick ="add()" ><span class="glyphicon glyphicon-plus"></span>Add Field</label></a>
                    </div>

                </div>

                <div class = "modal-footer">
                    <button type ="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span>ADD</button>
                </div>
        </form>
    </div>

</div>
</div>

