<style>
    .table td {
        font-size:15px;

    }
</style>
<span style="font-size:medium;">Current Load : </span><span style="font-size:Large;"> Prof. {{$instructorData[0]['name']}}</span>
<div class = "table-bordered">
    <table class="table table-borderless">
        <tr class=" TableHeader">
            <th>Subject Name</th>
            <th>Subject Code</th>
            <th>Section</th>
            <th>Semester</th>
            <th>SY</th>
            <th>Units</th>
            <th>Acton</th>
        </tr>
        @if($instructorLoad==null)  
    </table> 
</div>
<div class = "pull-left">
    <a  class = "btn btn-primary" data-toggle="modal" href="#myModal3">Add Load</a>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body" style = "padding:0px 2px 0px 2px;">
                <form method="post" action="../addload/{{$id}}" id="form1">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />\
                    <div class = "container col-lg-12">
                        <div id = "instructor" class ="form-group">
                            <div class="form-group col-lg-6 col-md-6 col-xs-6">
                                <label for="subject">Subject</label>
                                <input type="text"  placeholder="subject name" class="form-control" name="subject">
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="subcode">Subject Code</label>
                                <input type="text"  placeholder="subcode" class="form-control" name="subcode">
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-xs-4">
                                <label for="section">Section</label>
                                <input type="text"  placeholder="section" class="form-control" name="section">
                            </div>

                            <div class="form-group col-lg-6">
                                <label for="sy">School Year</label>
                                <input type="text"  placeholder="0000-0000" class="form-control" name="sy">
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="status">Semester</label>
                                <select  name = "semester" class="form-control" size = "1">
                                    <option>1st Semester</option>
                                    <option>2nd Semester</option>
                                    <option>Summer</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-4 col-xs-4">
                                <label for="section">Units</label>
                                <input type="text"  placeholder="units" class="form-control" name="units">
                            </div>
                        </div>
                    </div>


            </div>

            <div class = "modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>  Discard</button>
                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span>  Save</button>
            </div>
        </div>
        </form>
    </div>
</div>
@else
@foreach($instructorLoad as $data)
<tr class="tablebg">
    <td>{{$data['subject']}}</td>
    <td>{{$data['subj_code']}}</td>
    <td>{{$data['class']}}</td>
    <td>{{$data['semester']}}</td>
    <td>{{$data['school_year_sy']}}</td>
    <td>{{$data['units']}}</td>
    <td <form method="post" action="./records" id="form1">
            <a data-toggle = "modal" href="#{{$data['id']}}"> 
                </input><span class="glyphicon glyphicon-edit"></span>  Edit </a>
    </td>
</tr>
@endforeach

</table>

</div>
</br>
<div class = "pull-right">
    <a  class = "btn btn-primary" data-toggle="modal" href="#myModal3">Add Load</a>
</div>
</br>
@foreach($instructorLoad as $data)
<!-- Modal -->
<div class="modal fade" id="{{$data['id']}}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body" style = "padding:0px 2px 0px 2px;">
                <form method="post" action="../editload/{{$data['id']}}" id="form1">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <input type="hidden" name="id" value="{{$id}}" />
                    <div class = "container col-lg-12">
                        <div id = "instructor" class ="form-group">
                            <div class="form-group col-lg-6 col-md-6 col-xs-6">
                                <label for="subject">Subject</label>
                                <input type="text"  placeholder="subject name" value = "{{$data['subject']}}" class="form-control" name="subject">
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="subcode">Subject Code</label>
                                <input type="text"  placeholder="subcode" value = "{{$data['subj_code']}}" class="form-control" name="subcode">
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-xs-4">
                                <label for="section">Section</label>
                                <input type="text"  placeholder="section" value = "{{$data['class']}}" class="form-control" name="section">
                            </div>

                            <div class="form-group col-lg-6">
                                <label for="sy">School Year</label>
                                <input type="text"  placeholder="0000-0000" value = "{{$data['school_year_sy']}}" class="form-control" name="sy">
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="status">Semester</label>
                                <select  name = "semester" class="form-control" size = "1">
                                    @if($data['semester']=='1st Semester')
                                    <option>{{$data['semester']}}</option>
                                    <option>2nd Semester</option>
                                    <option>Summer</option>
                                    @endif
                                    @if($data['semester']=='2nd Semester')
                                    <option>{{$data['semester']}}</option>
                                    <option>1st Semester</option>
                                    <option>Summer</option>
                                    @endif @if($data['semester']=='Summer')
                                    <option>{{$data['semester']}}</option>
                                    <option>1st Semester</option>
                                    <option>2nd Semester</option>
                                    @endif

                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-4 col-xs-4">
                                <label for="section">Units</label>
                                <input type="text"  placeholder="units" class="form-control" value = "{{$data['units']}}" name="units">
                            </div>
                        </div>
                    </div>


            </div>
            <div class = "modal-footer">
                <button value = "delete" name = "action" type ="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>  Delete</button>
                <button value = "save" name = "action" type="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span>  Save</button>
            </div>
        </div>
        </form>
    </div>
</div>
@endforeach
<div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body" style = "padding:0px 2px 0px 2px;">
                <form method="post" action="../addload/{{$id}}" id="form1">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />\
                    <div class = "container col-lg-12">
                        <div id = "instructor" class ="form-group">
                            <div class="form-group col-lg-6 col-md-6 col-xs-6">
                                <label for="subject">Subject</label>
                                <input type="text"  placeholder="subject name" class="form-control" name="subject">
                            </div>
                            <div class="form-group col-lg-3">
                                <label for="subcode">Subject Code</label>
                                <input type="text"  placeholder="subcode" class="form-control" name="subcode">
                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-xs-4">
                                <label for="section">Section</label>
                                <input type="text"  placeholder="section" class="form-control" name="section">
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="sy">School Year</label>
                                <input type="text"  placeholder="0000-0000" class="form-control" name="sy">
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="status">Semester</label>
                                <select  name = "semester" class="form-control" size = "1">
                                    <option>1st Semester</option>
                                    <option>2nd Semester</option>
                                    <option>Summer</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-4 col-xs-4">
                                <label for="section">Units</label>
                                <input type="text"  placeholder="units" class="form-control" name="units">
                            </div>
                        </div>
                    </div>


            </div>

            <div class = "modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>  Discard</button>
                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span>  Save</button>
            </div>
        </div>
        </form>
    </div>
</div>
@endif