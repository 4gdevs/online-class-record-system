<div class="form-group col-lg-5">
    <label for="sname">Name:</label>
    <input type="text"  placeholder="name" class="form-control" name="sname">
</div>
<div class="form-group col-lg-4">
    <label for="uname">Email</label>
    <input type="text"  placeholder="email" class="form-control" name="email">
</div>
<div class="form-group col-lg-3">
    <label>User Type  </label> 
    <select  id ="utype" class="form-control" size = "1" name="userType">
        <option>Instructor</option>
        <option>Dean</option>
        <option>Chairman</option>
        <option>Admin</option>
    </select>
</div>
<div class="form-group col-lg-5">
    <label for="idNumber">Id No:</label>
    <input type="text"  placeholder="Id Number" class="form-control" name="idNumber">
</div>
<div class="form-group col-lg-4">
    <label for="contact">Contact Number</label>
    <input type="text"  placeholder="09#######" class="form-control" name="contact">
</div>
<div class="form-group col-lg-3">
    <label for="gender">Gender</label>
    <select  name = "gender" class="form-control" size = "1">
        <option>Male</option>
        <option>Female</option>
    </select>
</div>
