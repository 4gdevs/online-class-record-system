<div>
<style>
    body{
        padding:20px 20px;
    }

    .results tr[visible='false'],
    .no-result{
        display:none;
    }

    .results tr[visible='true']{
        display:table-row;
    }

    .counter{
        padding:8px; 
        color:#ccc;
    }
    .highlight_row{
        background-color:lightgrey;
    }

</style>
<div class = "form-group col-lg-2">
    <form method="post" action="./edituser" id="form1">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <label for=college""> Browse College</label>
        <select  class = "form-control " size = "1"onchange = "this.form.submit();" name="college">
            <option>College</option>
            <option>CEA</option>
            <option>CIIT</option>
            <option>CPSEM</option>
            <option>CAS</option>
        </select>
    </form>
</div>
<div class = "form-group col-lg-6 "style ="margin-top:17px;">
    <span style="font-size:25px;"> | {{$InstructorList[0]['college']}} | Faculty (s) </span>
</div>
<div class="form-group pull-right"style="margin-top:20px;">
    <input class = "search form-control" placeholder="What you looking for?"></input>
</div>
<div class="form-group col-lg-12"style="overflow-y:auto;height:150px;margin-right:100px;" >
    <span class="counter pull-right"></span>
    <table class = "table table-hover results" >
        <thead> 
            <tr class = "TableHeader">
                <th>ID Number</th>
                <th>Name</th>
                <th>Department</th>
                <th>Status</th>
                <th>UserType</th>
                <th>Action</tdh>
            </tr>
            <tr class="warning no-result">
                <td colspan="4"><i class="fa fa-warning"></i> No result</td>
            </tr>
        </thead> 

        <tbody style="overflow-y:auto;">
            @foreach($InstructorList as $data)
            <tr >
                <td>{{$data['id']}}</td>
                <td>{{$data['name']}}</td>
                <td>{{$data['department']}}</td>
                <td>{{$data['status']}}</td>
                <td>{{$data['userType']}}</td>
                <td><form method="post" action="./records" id="form1">
                        <a href="./viewload/{{$data['id']}}"> 
                            <span class = "glyphicon glyphicon-edit"></span> Open Load </a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function () {
        $(".search").keyup(function () {
            var searchTerm = $(".search").val();
            var listItem = $('.results tbody').children('tr').children('td');
            var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
            $.extend($.expr[':'], {'containsi': function (elem, i, match, array) {
                    return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                }
            });
            $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
                $(this).attr('visible', 'false');
            });
            $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
                $(this).attr('visible', 'true');
            });
            var jobCount = $('.results tbody tr[visible="true"]').length;
            $('.counter').text(jobCount + ' item');
            if (jobCount == '0') {
                $('.no-result').show();
            }
            else {
                $('.no-result').hide();
            }
        });
        $('.results tbody tr').click(function (event) {
            if (event.target.type !== 'checkbox') {
                $(':checkbox', this).trigger('click');
            }
        });
        $("input[type='checkbox']").change(function (e) {
            if ($(this).is(":checked")) {
                $(this).closest('tr').addClass("highlight_row");
            } else {
                $(this).closest('tr').removeClass("highlight_row");
            }
        });
    });
</script>
</div>