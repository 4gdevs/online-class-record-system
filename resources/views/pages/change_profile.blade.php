<!-- Modal -->
<div class="modal fade" id="myModal10" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-edit "></span> Edit profile</h4>
            </div>
            <div class="modal-body" style = "padding:0px 2px 0px 2px;">
                <form type = "hidden" method="post" action="./editprofile/{{Auth::user()->id}}" id="form1" />
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" /> 
                <div class = "container col-lg-12">
                    <div class="form-group col-lg-12">
                        <label class = "label label-primary"style = "font-size: medium;"for="idNumber">Basic Information</label>
                    </div>
                    <div class="form-group col-lg-5">
                        <label for="sname">Name:</label>
                        <input type="text"  value = "{{ Auth::user()->name }}" placeholder="name" class="form-control" name="sname">
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="uname">Email</label>
                        <input type="text"  value = "{{ Auth::user()->email }}" placeholder="email" class="form-control" name="email">
                    </div>

                    <div class="form-group col-lg-3">
                        <label for="gender">Gender</label>
                        <select  name = "gender" class="form-control" size = "1">
                            <option>{{ Auth::user()->gender }}</option>
                            <option>Male</option>
                            <option>Female</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-5">
                        <label for="contact">Contact Number</label>
                        <input value = "{{ Auth::user()->contactNumber }}"type="text"  placeholder="09#######" class="form-control" name="contact">
                    </div>    
                    <div class="form-group col-lg-4">
                        <label for="gender">Status</label>
                        <select  name = "status" class="form-control" size = "1">
                            <option>{{ Auth::user()->status }}</option>
                            <option>Full-Time</option>
                            <option>Part-Time</option>

                        </select>
                    </div>
                    <div class="form-group col-lg-12">
                        <label class = "label label-primary"style = "font-size: medium;"for="idNumber">Login Information</label>
                    </div>
                    <div class="form-group col-lg-5">
                        <label for="idNumber">Username:</label>
                        <input value = "{{ Auth::user()->username }}"type="text"  placeholder="Username" class="form-control" name="username">
                    </div>
                    <div class="form-group col-lg-4">
                        <label for="contact">Password</label>
                        <input type="password"  placeholder="****** "class="form-control" name="password">
                    </div>
                </div>
            </div>
            <div class = "modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>  Discard</button>
                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span>  Save</button>
            </div>
        </div>
        </form>
    </div>
</div>