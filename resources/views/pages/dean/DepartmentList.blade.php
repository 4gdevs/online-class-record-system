@if($departmentList[0]['college']=='CEA')
<label style = "font-size:large;">&nbsp; College Of Engineering And Architecture (CEA)&nbsp;  </label>
@endif
<br>
<br>
<div class = "container" style = "padding-left: 10%;">
    <div  class = "row col-lg-12">
        @foreach($departmentList as $data)
        <div class="col-lg-3">
            <form method="post" action="/school" id="form1">
                <a href = "../school/{{$data['department']}}">
                    @if($data['department']=='Architecture')
                    <img src = "../assets/img/{{$data['department']}}.png" style = "width:100px;height: 100px;"/>
                    <br>
                    <label style = "font-size: medium;">{{$data['department']}}</label>
                    @else
                    <img src = "../assets/img/{{$data['department']}}.jpg" style = "width:100px;height: 100px;"/>
                    <br>
                    <label style = "font-size: medium;padding-left: 16%;">{{$data['department']}}</label>
                    @endif

                </a>  

        </div>
        @endforeach
    </div>
</div>