
<div class="modal fade " id="mid{{$data['id']}}}}" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="padding:10px 10px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>   Midterm Remarks </h4>
            </div>
            <div class = "modal-body">

                <table class = "table table-hover" >
                    <tr class = "TableHeader">
                        <th>Student ID</th>
                        <th>Student Name</th>
                        <th>Midterm Grade</th>
                        <th>Remarks </th>
                    </tr>
                    @foreach($gradesData as $data)
                    <tr>

                        <td>{{$data['id']}}</td>
                        <td>{{$data['sname']}}</td>
                        <td>{{$data['midterm_grade']}}</td>
                        <td>{{$data['remarks']}}</td>
                    </tr>
                    @endforeach
                </table>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-success btn-danger"><span class = "glyphicon glyphicon-remove"></span> Cancel</button>
                    &nbsp; &nbsp;
                    <button type="submit" class="btn btn-success btn-success"><span class = "glyphicon glyphicon-share"></span> Post</button>
                </div>

            </div>
        </div>
    </div>
</div>