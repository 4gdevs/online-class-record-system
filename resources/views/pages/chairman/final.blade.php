<div class="modal fade " id="final{{$data['id']}}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form type = "hidden" method="post" action="../submit/{{$gradesData[0]['id']}}" id="form1" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

            <div class="modal-header" style="padding:20px 15px;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Final Remarks</h4>
            </div>
            <div class = "modal-body">
                <table class = "table table-borderless">
                    <tr class = "TableHeader">
                        <th>Student ID</th>
                        <th>Student Name</th>
                        <th>Midterm</th>
                        <th>Pre-Final </th>
                        <th>Final </th>
                        <th>Remarks </th>
                    </tr>
                    @foreach($gradesData as $data)
                    <tr>
                        <td>{{$data['id']}}</td>
                        <td>{{$data['sname']}}</td>
                        <td>{{$data['midterm_grade']}}</td>
                        <td>{{$data['prefinal_grade']}}</td>
                        <td>{{$data['final_grade']}}</td>
                        <td>{{$data['remarks']}}</td>
                    </tr>
                    @endforeach
                </table>

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-success btn-danger"><span class = "glyphicon glyphicon-remove"></span> Cancel</button>
                <button type="submit" class="btn btn-success btn-success"><span class = "glyphicon glyphicon-share"></span> Proceed</button>
            </div>
            </form>
        </div>

    </div>
</div>

