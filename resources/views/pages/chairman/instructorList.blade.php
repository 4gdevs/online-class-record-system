<span style = "font-size:medium;">Available instructor(s)</span> <span style="font-family: Verdana, Arial;font-size: 11pt;"> | </span> <span style="font-size: large;"> {{$department}} Department </span>
<table class = "table table-bordered">
    <tr class = "TableHeader" style="text-transform: capitalize"> 
        <th>Employee No.</th>
        <th >Name</th>
        <th >Status</th>
        <th >Browse</th>
    </tr>
    @foreach($instructors as $data)
    <tr>
        <td>{{$data['id']}}</td>
        <td>{{$data['name']}}</td>
        <td>{{$data['status']}}</td>
        <td><form method="post" action="./records" id="form1">
                <a href="../classlist/{{$data['id']}}"> 
                    </input><span class="glyphicon glyphicon-folder-open"></span> &nbsp;Open</a> 
        </td>

    </tr>
    @endforeach
</table>