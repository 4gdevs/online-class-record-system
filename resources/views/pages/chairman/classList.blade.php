<style>
    .table td {
        font-size:15px;

    }
</style>
<div>
    <form type = "hidden" method="post" action="../classlist/{{$instructorData[0]['id']}}" id="form1" />
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="instructorID" value="{{$instructorData[0]['id']}}" />
    <div class = "container col-lg-12" style="margin-right:8px;">
        <div class = "form-group col-lg-8" style="padding-left: 0px;padding-right: 0px;margin-top:20px;">
            @if($subjectData==null)
            <span style = "font-size:medium;">No available class for the selected SY & Quarter</span>
            @else
            <span  style = "font-size:medium;">Available class(s) for {{$subjectData[0]['school_year_sy']}}| {{$subjectData[0]['semester']}} |</span> <span style="font-size: large">Faculty : {{$instructorData[0]['name']}}</span>
            @endif
        </div>
        <div class = "form-group col-lg-2" style="padding-left: 0px;padding-right: 0px;margin-bottom:20px;">
            <label for="section">School Year</label>
            <select class="form-control"  size = "1" name="schoolYear">
                <option>SY</option>
                @foreach($load as $data)
                <option>{{$data['school_year_sy']}}</option>
                @endforeach
            </select> 
        </div>
        <div class = "form-group col-lg-2">            

            <label for="section">Term</label>
            <select class="form-control" size = "1"onchange = "this.form.submit();" name="semester">
                <option>Term</option>
                <option>1st Semester</option>
                <option>2nd Semester</option>
                <option>Summer</option>
            </select>
        </div>
        <table class = "table table-bordered">
            <tr class = "TableHeader"> 
                <th>Section</th>
                <th>Subject Code</th>
                <th>Subject Title</th>
                <th>Action</th>
            </tr>
            @foreach($subjectData as $data)
            <tr>

                <td>&nbsp;{{$data['class']}}</td>
                <td>&nbsp;{{ $data['subj_code'] }}</td>
                <td>&nbsp;{{ $data['subject'] }}</td>
                <td> 
                    <a  href="../midterm/{{$data['id']}}"> 
                        <span class="glyphicon glyphicon-file"></span> &nbsp;Midterm</a>  | 

                    <a  href="../prefinal/{{$data['id']}}"> 
                        <span class="glyphicon glyphicon-file"></span> &nbsp;Pre-Final</a> 
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</form>
</div>
