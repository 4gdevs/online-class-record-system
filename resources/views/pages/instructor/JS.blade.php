<script>
    var withlab = document.getElementById('withlab').value
    if (withlab == 0) {
        document.getElementById('total2lab').value = 100;
        document.getElementById('total3').value = 100;
    }
    else {
        document.getElementById('total2lab').value = 0;
        document.getElementById('total3').value = 0;
    }
    var midterm = document.getElementById('Midterm').value;
    var pre = document.getElementById('Pre').value;
    var percent = "%";
    if (midterm > 0 && pre > 0) {
        var total = +midterm + +pre;
        document.getElementById('total1').value = total;
        var tot1 = total;
        var tot2 = document.getElementById('total2').value;
        var tot3 = document.getElementById('total3').value;
        var tot4 = document.getElementById('total2lab').value;

        if (tot1 == 100 && tot2 == 100 && tot3 == 100 && tot4 == 100) {
            document.getElementById("buttn").disabled = false;
        }
        else {
            document.getElementById("buttn").disabled = true;
        }
        ;
    }
    else {
        document.getElementById('total1').value = "";
    }


    var lec = document.getElementById('lec').value;
    var lab = document.getElementById('lab').value;
    var percent1 = "%";
    if (lec > 0 && lab > 0) {
        var total1 = +lec + +lab;
        document.getElementById('total3').value = total1;
        var tot1 = document.getElementById('total1').value;
        var tot2 = document.getElementById('total2').value;
        var tot3 = total1;
        var tot4 = document.getElementById('total2lab').value;

        if (tot1 == 100 && tot2 == 100 && tot3 == 100 && tot4 == 100) {
            document.getElementById("buttn").disabled = false;
        }
        else {
            document.getElementById("buttn").disabled = true;
        }
        ;
    }



    function myFunction() {
        var midterm = document.getElementById('Midterm').value;
        var pre = document.getElementById('Pre').value;
        var percent = "%";
        if (midterm > 0 && pre > 0) {
            var total = +midterm + +pre;
            document.getElementById('total1').value = total;
            var tot1 = total;
            var tot2 = document.getElementById('total2').value;
            var tot3 = document.getElementById('total3').value;
            var tot4 = document.getElementById('total2lab').value;

            if (tot1 == 100 && tot2 == 100 && tot3 == 100 && tot4 == 100) {
                document.getElementById("buttn").disabled = false;
            }
            else {
                document.getElementById("buttn").disabled = true;
            }
            ;
        }
        else {
            document.getElementById('total1').value = "";
        }

    }
    function myFunction2() {
        var lec = document.getElementById('lec').value;
        var lab = document.getElementById('lab').value;
        var percent1 = "%";
        if (lec > 0 && lab > 0) {
            var total1 = +lec + +lab;
            document.getElementById('total3').value = total1;
            var tot1 = document.getElementById('total1').value;
            var tot2 = document.getElementById('total2').value;
            var tot3 = total1;
            var tot4 = document.getElementById('total2lab').value;

            if (tot1 == 100 && tot2 == 100 && tot3 == 100 && tot4 == 100) {
                document.getElementById("buttn").disabled = false;
            }
            else {
                document.getElementById("buttn").disabled = true;
            }
            ;
        }
        else {
            document.getElementById('total3').value = "";
        }

    }

    var tot = [];
    function checklecTotal(val) {
        var errss = false;
        var exist = false;
        var total1 = 0;
        for (var i = tot.length - 1; i >= 0; i--) {
            if (tot[i] == val) {
                exist = true;
                //alert('exist')
            }
        }
        if (!exist && val != 1) {
            tot[tot.length] = val;
            //alert('does\'nt exist')
        }

        for (var i = tot.length - 1; i >= 0; i--) {
            var perc = document.getElementById(tot[i]).value;
            total1 = +total1 + +perc;
            if (perc == "" || perc <= 0) {
            }
        }


        if (!errss) {
            document.getElementById('total2').value = total1;
            var tot1 = document.getElementById('total1').value;
            var tot2 = total1;
            var tot3 = document.getElementById('total3').value;
            var tot4 = document.getElementById('total2lab').value;

            if (tot1 == 100 && tot2 == 100 && tot3 == 100 && tot4 == 100) {
                document.getElementById("buttn").disabled = false;
            }
            else {
                document.getElementById("buttn").disabled = true;
            }
            ;
        }
        else {
            document.getElementById('total2').value = "";
        }
    }

    var totlab = [];
    function checklabTotal(val) {
        var errss = false;
        var exist = false;
        var total1 = 0;

        for (var i = totlab.length - 1; i >= 0; i--) {
            if (totlab[i] == val) {
                exist = true;
                //alert('exist')
            }
        }
        if (!exist && val != 1) {
            totlab[totlab.length] = val;
            //alert('does\'nt exist')
        }

        for (var i = totlab.length - 1; i >= 0; i--) {
            var perc = document.getElementById(totlab[i]).value;
            total1 = +total1 + +perc;
            if (perc == "" || perc <= 0) {
            }
        }


        if (!errss) {
            document.getElementById('total2lab').value = total1;
            var tot1 = document.getElementById('total1').value;
            var tot2 = document.getElementById('total2').value;
            var tot3 = document.getElementById('total3').value;
            var tot4 = total1;

            if (tot1 == 100 && tot2 == 100 && tot3 == 100 && tot4 == 100) {
                document.getElementById("buttn").disabled = false;
            }
            else {
                document.getElementById("buttn").disabled = true;
            }
            ;
        }
        else {
            document.getElementById('total2').value = "";
        }
    }

    function removelec(val) {
        var minus = 0;

        $('#' + val + 'tr').remove();

        $('#selectAct').append('<option>' + val + '</option>');

        for (var i = tot.length - 1; i >= 0; i--) {

            if (tot[i] == val) {
                tot.splice(i, 1);
                checklecTotal(1)

            }
        }
        document.getElementById('deletable').value = val;

        $("#form-add-setting").submit();
    }

    function removelab(val) {
        $('#' + val + 'labtr').remove();
        $('#selectlab').append('<option>' + val + '</option>');

        for (var i = totlab.length - 1; i >= 0; i--) {
            if (totlab[i] == val) {
                totlab.splice(i, 1);
                checklabTotal(1)
            }
        }
        document.getElementById('deletable').value = val;
        $("#form-add-setting").submit();
    }

    function functionAdd() {
        var act = $('#selectAct').val();
        if (act != 'Select') {
            $('#myTable').append('<tr id="' + act + 'tr" class="child"><td>' + act + '</td><td><input id = "' + act + '" name = "' + act + '" type="text"style = "border:0;width: 80px;" Placeholder="%" onchange="checklecTotal(\'' + act + '\')" onclick="checklecTotal(\'' + act + '\')"></td><td><input style = "line-height: 150px;height: 25px;font-size: 13px;" type="color" name="' + act + 'color" value="#ffffff"></td><td><input style = "line-height: 150px;height: 25px;font-size: 13px;" type="color" name="' + act + 'fontcolor" value="#000000"></td><td><a href = "#"><span id="' + act + 'Del" value="' + act + 'Rem" onclick="removelec(\'' + act + '\')" class = "glyphicon glyphicon-remove"></span></a></td></tr>');

            document.getElementById("buttn").disabled = true;
            if (act != "Select") {
                $('#selectAct option:selected').remove();
            }
        }

    }
    ;

    function functionAddlab() {
        var act = $('#selectlab').val();
        if (act != 'Select') {
            $('#myTablelab').append('<tr id="' + act + 'tr" class="child"><td>' + act + '</td><td><input id = "' + act + '" name = "' + act + 'lab" type="text"style = "border:0;width: 80px;" Placeholder="%" onchange="checklabTotal(\'' + act + '\')" onclick="checklabTotal(\'' + act + '\')"></td><td><input style = "line-height: 150px;height: 25px;font-size: 13px;" type="color" name="' + act + 'labcolor" value="#ffffff"></td><td><input style = "line-height: 150px;height: 25px;font-size: 13px;" type="color" name="' + act + 'labfontcolor" value="#000000"></td><td><a href = "#"><span id="' + act + 'Del" onclick="removelab(\'' + act + '\')" class = "glyphicon glyphicon-remove"></span></a></td></tr>');

            document.getElementById("buttn").disabled = true;
            if (act != "Select") {
                $('#selectlab option:selected').remove();
            }
        }
    }
    ;

    $(document).ready(function () {

        $("#buttn").click(function () {
            $("form1").submit();
        });

        $('#checklab').checked(function () {
            $('#myTable2').append('');
        });

    });

    $('#checklab').change(function () {
        if (!this.checked) {
            //
            $('#divlab').fadeOut('slow');
            $('#inputlab').fadeOut('slow');
            $('#labbox').fadeOut('slow');
            $('#TermExam-lablabtr').remove();
            document.getElementById('total2lab').value = 100;
            document.getElementById('total3').value = 100;

        }
        else {
            $('#divlab').fadeIn('slow');
            $('#inputlab').fadeIn('slow');
            $('#labbox').fadeIn('slow');
            checklabTotal(1);
            myFunction2();
        }
    });


    $('#form-add-setting').on('submit', function () {

        $.post(
                $(this).prop('action'),
                {
                    "_token": $(this).find('input[name=_token]').val(),
                    "deletable": $('#deletable').val()
                },
        function (data) {
            alert(data)
        },
                'json'
                );

        return false;
    });

</script>