<label style ="font-size:18px; margin-left:15px;">{{$studentEnrolledData[0]['subject']}} - {{$studentEnrolledData[0]['class']}}</label>

<div class="form-group col-lg-12"style="overflow-y:auto;height:200px;margin-right:100px;" >
    <table class = "table table-hover" >
        <thead> 
            <tr class = "TableHeader">
                <th>Student ID</th>
                <th>Student Name</th>
                <th>Course</th>
                <th>Class</th>
                <th>Gender</th>
                <th>Action</th>
            </tr>
        </thead> 
        <tbody style="overflow-y:auto;">
            @foreach($studentEnrolledData as $data)
            <tr>
                <th scope = "row">{{$data['id']}}</th>
                <td>{{$data['sname']}}</td>
                <td>{{$data['course']}} - {{$data['year']}}</td>
                <td>{{$data['class']}}</td>
                <td>{{$data['gender']}}</td>
                <td><a data-toggle="modal" href="#{{$data['id']}}remove"> 
                        </input><span class="glyphicon glyphicon-remove"></span>  remove </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<form method ="get" role="form" action = "../enrollstudent/{{$id}}" id ="form2">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

    <button class = "btn btn-primary"> Add Student to this class.</button>
</form>
@foreach($studentEnrolledData as $data)
<div class="modal fade" id="{{$data['id']}}remove" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form method="post" action="../remove/{{$data['id']}}" id="form1" >
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <input type="hidden" name="load_id" value="{{$studentEnrolledData[0]['load_id']}}" />

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><span class="glyphicon glyphicon-remove-sign "></span> Remove Student</h4>
                </div>
                <div class="modal-body" style = "padding:0px0px 0px 0px;">
                    <h5> Are your sure you want to remove <strong>{{$data['sname']}}</strong> from this class?</h5>
                </div>

                <div class = "modal-footer">
                    <strong style = "margin-right:125px;">Note: All the records will be removed.</strong>
                    <button data-dismiss="modal" class="btn btn-success"><span class="glyphicon glyphicon-remove"></span>  Discard</button>
                    <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-ok"></span>  OK</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endforeach
