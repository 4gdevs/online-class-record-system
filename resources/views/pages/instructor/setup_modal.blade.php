<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->

        <form method="post" action="{{ url('/setup') }}" id="form1">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><span class="glyphicon glyphicon-wrench "></span> Settings</h4>
                </div>
                <div class="modal-body" style = "padding:0px 20px 0px 20px;">

                    <h5 for="activity"><span class="glyphicon glyphicon-file">activity:</span>
                        <select id = "selectAct" name="selectAct" style = "line-height: 150px;height: 25px;font-size: 11px;">
                            <option>Select</option>
                            @foreach($act_list as $data)
                            <option>{{$data}}</option>
                            @endforeach

                        </select> &nbsp;

                        <button type="button" onclick="functionAdd()">Add</button>
                        &nbsp;
                        <input name="checklab" id="checklab" type="checkbox" {{$withlab!=0?'checked':''}}> Include Laboratory
                    </h5>
                    <input id="withlab" type="hidden" value="{{$withlab}}">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <div class="panel-group" style = "padding:0px 2px 0px 2px;">
                        <table id = "myTable" class = "table table-condensed">
                            <tbody>
                                <tr class = "TableHeader"> 
                                    <th>Activity</th>
                                    <th>Percentage</th>
                                    <th>Column Color</th>
                                    <th>Font Color</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($act_detail as $data)
                                @if($data['type']=='lec')
                                <tr id="{{$data['act_name'].'tr'}}" class="child"><td>{{$data['act_name']}}</td><td><input id = "{{$data['act_name']}}" name = "{{$data['act_name']}}" value="{{$data['percentage']}}" type="text"style = "border:0;width: 80px;" Placeholder="%" onkeypress="{{'checklecTotal(\''.$data['act_name'].'\')'}}" onmousemove="{{'checklecTotal(\''.$data['act_name'].'\')'}}" onchange="{{'checklecTotal(\''.$data['act_name'].'\')'}}"></td><td><input style = "line-height: 150px;height: 25px;font-size: 13px;" type="color" name="{{$data['act_name']}}color" value="{{$data['color']}}"></td><td><input style = "line-height: 150px;height: 25px;font-size: 13px;" type="color" name="{{$data['act_name']}}fontcolor" value="{{$data['fontcolor']}}"></td><td><a href = "#"><span id="{{$data['act_name']}}Del" onclick="{{'removelec(\''.$data['act_name'].'\')'}}" class = "glyphicon glyphicon-remove"></span></a></td></tr>
                                @endif
                                @endforeach
                            </tbody>

                            <tr>
                                <td style= "font-weight:bold">Total</td>
                                <td style= "font-weight:bold"><input style="width:40px;border:0;" readonly Placeholder="%"  id="total2"></td>
                                <td contenteditable="false"></td>
                                <td contenteditable="false"></td>
                            </tr>

                        </table>
                    </div>

                    <div {{$withlab==0?'hidden':''}} id="inputlab">
                        <h5 for="activity"><span class="glyphicon glyphicon-file">activity:</span>
                            <select id = "selectlab" name="act_name" style = "line-height: 150px;height: 25px;font-size: 11px;">
                                <option>Select</option>
                                @foreach($act_lablist as $data)
                                <option>{{$data}}</option>
                                @endforeach

                            </select> &nbsp;

                            <button type="button" onclick="functionAddlab()">Add</button>
                    </div>

                    <div {{($withlab==0)?'hidden':''}} id="divlab" class="panel-group" style = "padding:0px 2px 0px 2px;">
                        <table id = "myTablelab" class = "table table-condensed">
                            <tbody>
                                <tr class = "TableHeader"> 
                                    <th>Activity</th>
                                    <th>Percentage</th>
                                    <th>Column Color</th>
                                    <th>Font Color</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($act_detail as $data)
                                @if($data['type']=='lab')
                                <tr id="{{$data['act_name'].'labtr'}}" class="child"><td>{{$data['act_name']}}</td><td><input id = "{{$data['act_name']}}" name = "{{$data['act_name'].'lab'}}" value="{{$data['percentage']}}" type="text"style = "border:0;width: 80px;" Placeholder="%" onchange="{{'checklabTotal(\''.$data['act_name'].'\')'}}" onkeypress="{{'checklabTotal(\''.$data['act_name'].'\')'}}" onmousemove="{{'checklabTotal(\''.$data['act_name'].'\')'}}"></td><td><input style = "line-height: 150px;height: 25px;font-size: 13px;" type="color" name="{{$data['act_name']}}labcolor" value="{{$data['color']}}"></td><td><input style = "line-height: 150px;height: 25px;font-size: 13px;" type="color" name="{{$data['act_name']}}labfontcolor" value="{{$data['fontcolor']}}"></td><td><a href = "#"><span id="{{$data['act_name']}}Del" onclick="{{'removelab(\''.$data['act_name'].'\')'}}" class = "glyphicon glyphicon-remove"></span></a></td></tr>
                                @endif
                                @endforeach
                            </tbody>

                            <tr>
                                <td style= "font-weight:bold">Total</td>
                                <td style= "font-weight:bold"><input style="width:40px;border:0;" readonly Placeholder="%"  id="total2lab"></td>
                                <td contenteditable="false"></td>
                                <td contenteditable="false"></td>
                            </tr>

                        </table>
                    </div>

                    <strong class="warning">Note : All Total must be equal to 100%.</strong>
                    <div style="width: 250px;margin-right: 20px" class="pull-right">
                        <div class = "form-group col-lg-6 col-md-6 col-xs-6 pull-right" style = "margin-top:10px;" >
                            <table class = "table table-bordered " style = "width:30%;" >
                                <tr class = "TableHeader">
                                    <th>&nbsp;Term </th>
                                    <th>%</th>
                                </tr>
                                <tr>
                                    <td >MidTerm</td>
                                    <td><input type="text" Placeholder="%" name="Midterm" id="Midterm" style = "width:100%;height: 100%;border:0;" value="{{$dmidterm}}" onchange="myFunction()" ></td>
                                </tr>
                                <tr>
                                    <td>PreFinal</td>
                                    <td><input type="text" Placeholder="%" name="Pre" id="Pre" style = "width:100%;height: 100%;border:0;" value="{{$dpre}}" onchange="myFunction()"></td>
                                </tr>
                                <tr>
                                    <td style= "font-weight:bold" >Total</td>
                                    <td><input style="width: 40px;border:0;" readonly Placeholder="%"  id="total1"></td>
                                </tr>
                            </table>
                        </div>
                        <div id="labbox" {{$withlab==0?'hidden':''}} class = "form-group col-lg-6 col-md-6 col-xs-6" style = "margin-top:10px;" >
                            <table class = "table table-bordered " style = "width:30%;" >
                                <tr class = "TableHeader">
                                    <th>&nbsp;Category </th>
                                    <th>%</th>
                                </tr>
                                <tr>
                                    <td>Lecture</td>
                                    <td><input type="text" Placeholder="%" name="lec" id="lec" style = "width:100%;height: 100%;border:0;" value="{{$dlec}}" onchange="myFunction2()"></td>
                                </tr>
                                <tr>
                                    <td>Lab</td>
                                    <td><input type="text" Placeholder="%" name="lab" id="lab" style = "width:100%;height: 100%;border:0;" value="{{$dlab}}" onchange="myFunction2()"></td>
                                </tr>
                                <tr>
                                    <td><label Placeholder="100%" </label></td>
                                    <td><input style="width: 40px;border:0;" readonly Placeholder="%"  id="total3"></td>
                                </tr>


                            </table>
                        </div>
                        <div class="pull-right">
                            <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>  Discard</button>
                            <button id="buttn" onclick="Submit1()" type="submit" class="btn btn-success" disabled><span class="glyphicon glyphicon-save"></span>  Save</button>
                        </div>
                    </div>

                </div>
                <div class = "modal-footer">

                </div>
            </div>
        </form>
        {!! Form::open( array(
        'route' => 'settings.create',
        'method' => 'post',
        'id' => 'form-add-setting'
        ) ) !!}
        <input type="hidden" id="deletable" name="deletable" />
        {!! Form::close() !!}

    </div>
</div>
@foreach($act_list as $act_list)
<script type="text/javascript">
    $(this).siblings('select').find('option[value="' + $(this).val() + '"]').remove();
</script>
@endforeach
@include('pages.instructor.JS')