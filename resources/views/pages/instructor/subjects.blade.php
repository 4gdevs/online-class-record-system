<style>
    .table td {
        font-size:15px;

    }
</style>
<form type = "hidden" method="post" action="{{ url('/instructor') }}" id="form1" />
<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
<div class = "form-group col-lg-8" style="padding-left: 0px;padding-right: 0px;">
    <br>
    <br>
    @if($subjectData==null)
    <span style = "font-size:medium;">No available class for the selected SY & Quarter </span>
    @else
    <span  style = "font-size:medium;">Available class(s) for {{$subjectData[0]['school_year_sy']}}| {{$subjectData[0]['semester']}} </span>
    @endif
</div>
<div class = "form-group col-lg-2">
    <label for="section">School Year</label>
    <select class="form-control"  size = "1" name="schoolYear">
        <option>School Year</option>
        @foreach($load as $data)
        <option>{{$data['school_year_sy']}}</option>
        @endforeach
    </select> 
</div>
<div class = "form-group col-lg-2">            
    <label for="section">Change Term</label>
    <select class="form-control" size = "1"onchange = "this.form.submit();" name="semester">
        <option>Term</option>
        <option>1st Semester</option>
        <option>2nd Semester</option>
        <option>Summer</option>
    </select>
</div>
<div id="table1" class="table-editable">

    <table class = "table table-bordered">
        <tr class = "TableHeader"> 
            <th>Section</th>

            <th>Subject Code</th>
            <th>Subject Title</th>
            <th>Action</th>
        </tr>
        @foreach($subjectData as $data)
        <tr>

            <td>&nbsp;{{$data['class']}}</td>
            <td>&nbsp;{{ $data['subj_code'] }}</td>
            <td>&nbsp;{{ $data['subject'] }}</td>
            @if($data['setup_flag']=='N')
            <td <form method="post" action="./records" id="form1">
                    <a href="./enrollstudent/{{$data['id']}}"> 
                        <span class="glyphicon glyphicon-wrench"></span>  Setup </a>
            </td>
            @else
            <td <form method="post" action="./records" id="form1">
                    <a href="./class/{{$data['id']}}"> 
                        </input><span class="glyphicon glyphicon-folder-open"></span>  Open </a> | <form method="post" action="./records" id="form1">
                        <a href="./editclass/{{$data['id']}}"> 
                            <span class="glyphicon glyphicon-wrench"></span>  Edit Class </a>
                        </td>
                        @endif
                        </tr>
                        @endforeach
                        </table>
                        </div>
                    </form>
