<style>
    body{
        padding:20px 20px;
    }

    .results tr[visible='false'],
    .no-result{
        display:none;
    }

    .results tr[visible='true']{
        display:table-row;
    }

    .counter{
        padding:8px; 
        color:#ccc;
    }
    .highlight_row{
        background-color:lightgrey;
    }

</style>
<form method="post" action="../fetchdata/{{$id}}" id="form2">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <div class="form-group col-lg-3"style="margin-top:20px;">
        <input name = "searchkey"class = "form-control" placeholder="Enter any for course"/>
    </div>
    <div class="form-group col-lg-3"style="margin-top:20px;">
        <button type = "submit" class = "btn btn-primary"> Fetch Data</button>
    </div>
</form>
<div class="form-group pull-right"style="margin-top:20px;">
    <input class = "search form-control" placeholder="What you looking for?"></input>
</div>
<form method="post" action="../enrollstudents/{{$id}}" id="form1">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
    <div class="form-group col-lg-12"style="overflow-y:auto;height:200px;margin-right:100px;" >
        <span class="counter pull-right"></span>
        <table class = "table table-bordered results" >
            <thead> 
                <tr class = "TableHeader">
                    <th>Student ID</th>
                    <th>Student Name</th>
                    <th>Course</th>
                    <th>Add</th>
                </tr>
                <tr class="warning no-result">
                    <td colspan="4"><i class="fa fa-warning"></i> No result</td>
                </tr>
            </thead> 
            @if($studentData==null)
            <tr>
                <td>No data to display</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @else
            <tbody style="overflow-y:auto;">
                @foreach($studentData as $data)
                <tr>
                    <th scope = "row">{{$data['id']}}</th>
                    <td>{{$data['sname']}}</td>
                    <td>{{$data['course']}} - {{$data['year']}}</td>
                    <td><input name ="check{{$data['id']}}" type ="checkbox" value ="{{$data['id']}}" />
                    </td>
                </tr>
                @endforeach
            </tbody>
            @endif
        </table>
    </div>

    <div class = "pull-right">
        <button type = "submit"class = "btn btn-success" > Enroll Now!</button>
    </div>
</form>
</br>
</br>
</br>
</br>
<script>
    $(document).ready(function () {
        $(".search").keyup(function () {
            var searchTerm = $(".search").val();
            var listItem = $('.results tbody').children('tr').children('td');
            var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
            $.extend($.expr[':'], {'containsi': function (elem, i, match, array) {
                    return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                }
            });
            $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
                $(this).attr('visible', 'false');
            });
            $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
                $(this).attr('visible', 'true');
            });
            var jobCount = $('.results tbody tr[visible="true"]').length;
            $('.counter').text(jobCount + ' item');
            if (jobCount == '0') {
                $('.no-result').show();
            }
            else {
                $('.no-result').hide();
            }
        });
        $('.results tbody tr').click(function (event) {
            if (event.target.type !== 'checkbox') {
                $(':checkbox', this).trigger('click');
            }
        });
        $("input[type='checkbox']").change(function (e) {
            if ($(this).is(":checked")) {
                $(this).closest('tr').addClass("highlight_row");
            } else {
                $(this).closest('tr').removeClass("highlight_row");
            }
        });
    });
</script>