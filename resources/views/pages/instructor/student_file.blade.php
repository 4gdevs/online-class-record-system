
<div class = "form-group col-lg-8" style = "margin-top:15px;">
    <span class="clTitle">Class Records</span><span style="font-family: Verdana, Arial;font-size: 11pt;"> | {{$SY}}</span><span style="font-family: Verdana, Arial;font-size: 11pt;"> | {{$sem}}</span><span style="font-family: Verdana, Arial;font-size: 11pt;font-weight:bold;"> | {{$term}}</span>
</div>
<div class = "form-group col-lg-2" style = "margin-left:185px;">
    <form method="post" action="../class/{{$subject_id}}" id="form1">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <label for="section">Change Term</label>
        <select class = "form-control" name="term"  onchange="this.form.submit();">
            <option>Change Term</option>   
            <option>Midterm</option>    
            <option>Pre-Final</option>
        </select>
</div>
</form>
@if($activitiesData==null || @activitiesDatalab ==null)
<div id="table" class="table-responsive">
    <table class = "table table-bordered">
        <tr class = "TableHeader" style="text-transform: capitalize"> 
            <th style="height: 47px">ID No.</th>
            <th style="height: 47px">Name</th>
            <th style="height: 47px">Grade</th>
            <th style="height: 47px">Remarks</th>
        </tr>
        <tr>
            <td></td>

            <td>No Data To Display</td>
            <td></td>
            <td></td>

        </tr>
    </table>
    <div class = pull-right style="margin-bottom: 15px">
        <button id="export-btn" style="height: 30px;"class="btn btn-info" disabled="true"><span class="glyphicon glyphicon-download-alt"></span> Import</button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <button id="export-btn" style="height: 30px;"class="btn btn-success"disabled="true"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" data-toggle = "modal" data-target = "#myModal1" style  = "height: 30px;" class="btn btn-warning"disabled="true"><span class = "glyphicon glyphicon-open"></span> Post</button>
    </div>
</div>
@else
<div class = "container col-lg-12">
    <div id="table" class="table-responsive">
        <form method="post" action="{{ url('/update') }}" id="updateform" >

            <div style="margin:0;overflow-y: auto;height: 450px;margin-bottom: 20px" class="form-group">
                <table class = "table table-bordered form-group col-lg-3 col-md-3 col-xs-3" style="width: 262px; margin-left: 15px;">

                    <tr class = "TableHeader" style="text-transform: capitalize;"> 
                        <th style="height: 30px">ID No.</th>
                        <th style="height: 30px">Name</th>
                    </tr>
                    <tr class="TableHeader" style="background-color: #007F64;" style="text-transform: capitalize;"> 
                        <td>Total</td>
                        <td>Points</td>
                    </tr>
                    <?php
                    $activitiesNo = 1;
                    $recordsNo = 1;
                    ?>
                    @foreach($activitiesData as $data)

                    <tr>

                        <td class="TableHeader" style="background-color: #449d44;" contenteditable="false">{{$data[0]['students_id']}}</td>
                        <td style="background-color: #FCFB98; " contenteditable="false">{{$data[0]['sname']}}</td>


                    </tr>
                    @endforeach
                    <tr class = "TableHeader" style="text-transform: capitalize"> 
                        <th style="height: 5px;background-color: #007F64;"></th>
                        <th style="height: 5px;background-color: #007F64;"></th>
                    </tr>
                </table>
                <div class = "table table-bordered form-group col-lg-3 col-md-3 col-xs-3" style="overflow-x: auto; width: 450px; padding: 0px; border: 0px">
                    <table class = "table table-bordered" style="margin-bottom: -5px;border-bottom-width: 0px;margin-bottom: 0px;">

                        <tr class = "TableHeader" style="text-transform: capitalize"> 

                            <?php
                            $count = 0;
                            $end = 'false';
                            ?>
                            @foreach($activitiesData as $data)
                            <?php if ($end == 'false'):
                                ?>
                                @foreach($data as $data1)
                                <th style="height: 30px">{{$data[$count++]['act_name']}}</th>
                                @endforeach
                                <?php
                            endif;
                            $end = 'true';
                            ?>
                            @endforeach
                            <?php
                            $count = 0;
                            $end = 'false';
                            ?>
                            @foreach($activitiesData as $data)
                            <?php if ($end == 'false'):
                                ?>
                                <th nowrap style="height: 30px">{{$data[0]['term']}} Exam</th>
                                @if($withlab!=0)
                                <th nowrap style="height: 30px">{{$data[0]['term']}} Exam (lab)</th>
                                @endif
                                <?php
                            endif;
                            $end = 'true';
                            ?>
                            @endforeach
                        </tr>
                        <tr style="background-color: #007F64;" style="text-transform: capitalize">

                            <?php
                            $count = 0;
                            $end = 'false';
                            ?>
                            @foreach($activitiesData as $data)
                            <?php if ($end == 'false'):
                                ?>
                                @foreach($data as $data1)
                                <td><input data-toggle="tooltip" data-placement="top" title="{{$data1['act_name']}} Total" style="background-color: transparent;color: #ffffff;height: 15px;font-size:14px; width: 54px; border: 0;padding: 0px 0px 0px;"  name="{{ $data1['act_name'] }}" value="{{ $data[$count++]['total'] }}"></td>
                                @endforeach
                                <td><input data-toggle="tooltip" data-placement="top" title="{{$data[0]['term']}} Exam Total" style="background-color: transparent;color: #ffffff;height: 15px;font-size:14px; width: 54px; border: 0;padding: 0px 0px 0px;"  name="{{ $data[0]['records_id'] }}total" value="{{ $data[0]['exam_total'] }}"></td>
                                @if($withlab!=0)
                                <td><input data-toggle="tooltip" data-placement="top" title="{{$data[0]['term']}} Exam Total (lab)" style="background-color: transparent;color: #ffffff;height: 15px;font-size:14px; width: 54px; border: 0;padding: 0px 0px 0px;"  name="{{ $data[0]['records_id'] }}total (lab)" value="{{ $data[0]['examlab_total'] }}"></td>
                                @endif
                                <?php
                            endif;
                            $end = 'true';
                            ?>
                            @endforeach

                        </tr>


                        @foreach($activitiesData as $data)
                        <tr>
                            <?php
                            $count = 1;
                            ?>

                            @foreach($data as $data1)
                            <td bgcolor="{{$data1['color']}}"><input data-toggle="tooltip" data-placement="top" title="{{$data1['act_name']}}" tabindex="{{$count++}}" style="color: {{$data1['fontcolor']}}; background-color: transparent;height: 15px;font-size:14px; width: 80px; border: 0;padding: 0px 0px 0px;"  name="{{ $data1['id'] }}" value="{{ $data1['score'] }}"></td>
                        <input type="hidden" name="activities{{$activitiesNo}}" value="{{$data1['id']}}" />
                        <input type="hidden" name="actname{{$activitiesNo}}" value="{{$data1['act_name']}}" />
                        <?php
                        $activitiesNo++;
                        ?>
                        @endforeach
                        <td bgcolor="{{$data1['color']}}" contenteditable="false" ><input data-toggle="tooltip" data-placement="top" title="{{$data[0]['term']}} Exam Score" style="background-color: transparent;height: 15px;font-size:14px; width: 54px; border: 0;padding: 0px 0px 0px;"  name="{{ $data[0]['records_id'] }}score" value="{{ $data[0]['exam_score'] }}"></td>
                        @if($withlab!=0)
                        <td bgcolor="{{$data1['color']}}" contenteditable="false" ><input data-toggle="tooltip" data-placement="top" title="{{$data[0]['term']}} Exam Score (lab)" style="background-color: transparent;height: 15px;font-size:14px; width: 54px; border: 0;padding: 0px 0px 0px;"  name="{{ $data[0]['records_id'] }}score (lab)" value="{{ $data[0]['examlab_score'] }}"></td>
                        @endif
                        </tr>
                        <input type="hidden" name="records{{$recordsNo}}" value="{{$data1['records_id']}}" />
                        <?php $recordsNo++; ?>
                        @endforeach

                        <tr class = "TableHeader" style="text-transform: capitalize"> 
                            <?php
                            $count = 0;
                            $end = 'false';
                            ?>
                            @foreach($activitiesData as $data)
                            <?php if ($end == 'false'):
                                ?>
                                @foreach($data as $data1)

                                <th style="height: 5px;background-color: #007F64;"></th>
                                @endforeach
                                <?php
                            endif;
                            $end = 'true';
                            ?>
                            @endforeach
                            <th style="height: 5px;background-color: #007F64;"></th>
                            @if($withlab!=0)
                            <th style="height: 5px;background-color: #007F64;"></th>
                            @endif
                        </tr>

                    </table>
                </div>


                <div class = "table table-bordered form-group col-lg-1 col-md-1 col-xs-1" style="overflow-x: auto; width: 30px; padding: 0px; border: 0px">
                    <table class = "table table-bordered" style="margin-bottom: -5px;border-bottom-width: 0px;margin-bottom: 0px;">
                        <th style="height: 32px;background-color: white;"><a data-toggle = "modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span></a></th>
                        <tr class="TableHeader" style="background-color: lightgrey;">
                            <td style="height: 32px;background-color: #007F64;"></td>
                        </tr>
                        @foreach($activitiesData as $data)

                        @foreach($data as $data1)

                        @endforeach
                        <tr>
                            <td style="height: 32px;"></td>
                        </tr>
                        @endforeach


                        <tr>
                            <td style="height: 5px;background-color: #007F64;"></td>
                        </tr>
                    </table>
                </div>
                <div class = "table table-bordered form-group col-lg-2 col-md-2 col-xs-2" style="overflow-x: auto; width: 180px; padding: 0px; border: 0px">
                    <table class = "table table-bordered" style="margin-bottom: -5px;border-bottom-width: 0px;margin-bottom: 0px;">
                        @if($gradeslist==null)

                        @else
                        <tr class = "TableHeader" style="text-transform: capitalize;height: 64px;"> 
                            <?php $end = false; ?>
                            @foreach($gradeslist as $data)
                            @foreach($data as $data2)
                            <?php if (!$end):
                                ?>
                                <th style="height: 47px;">{{$data2['Name']=='TermExam'?$term.' Exam':$data2['Name']}} Grade</th>
                                <?php
                            endif;
                            ?>
                            @endforeach
                            <?php $end = true; ?>

                            @endforeach
                        </tr>

                        </tr>
                        @foreach($gradeslist as $data)
                        <tr>
                            @foreach($data as $data2)
                            <td  style="background-color: #FFFFFF;"contenteditable="false"><input readonly data-toggle="tooltip" data-placement="top" title="Grade" style="background-color: transparent;height: 15px;font-size:14px; width: 80px; border: 0;padding: 0px 0px 0px;" value="{{$data2['Grade']}}"></td>
                            @endforeach
                        </tr>
                        @endforeach

                        <?php
                        $count = 0;
                        $end = 'false';
                        ?>
                        @foreach($gradeslist as $data)

                        @foreach($data as $data2)

                        <?php if ($end == 'false'):
                            ?>
                            <td style="height: 5px;background-color: #007F64;"></td>
                            <?php
                        endif;
                        ?>
                        @endforeach
                        <?php
                        $end = 'true';
                        ?>
                        @endforeach
                        </tr>
                        @endif
                    </table>
                </div>


                <div style="overflow-x: auto;">
                    <table class = "table table-bordered form-group col-lg-3 col-md-3 col-xs-3" style="width: 150px;margin-bottom: 0px;">
                        <tr class = "TableHeader" style="text-transform: capitalize;height: 64px;">




                            <?php
                            $count = 0;
                            $end = 'false';
                            ?>
                            @foreach($activitiesData as $data)
                            <?php if ($end == 'false'):
                                ?>
                                <th style="height: 47px">{{$data[0]['term']}} Grade</th>
                                <?php
                            endif;
                            $end = 'true';
                            ?>
                            @endforeach
                            <th>Remarks</th>

                        </tr>
                        <tr class="TableHeader" style="background-color: lightgrey;" style="text-transform: capitalize;">

                        </tr>
                        <?php $count = 0; ?>
                        @foreach($termgrade as $data)
                        <tr>
                            <?php $count++; ?>
                            <td  style="background-color: #FCFB98;"contenteditable="false"><input readonly data-toggle="tooltip" data-placement="top" title="{{$term}} Grade" style="background-color: transparent;height: 15px;font-size:14px; width: 54px; border: 0;padding: 0px 0px 0px;" value="{{$data['Grade']}}"></td>
                            <td  style="background-color: #a9d86e;"contenteditable="false"><input readonly data-toggle="tooltip" data-placement="top" title="{{$term}} Remarks" style="background-color: transparent;height: 15px;font-size:14px; width: 54px; border: 0;padding: 0px 0px 0px;" value="{{$data['Grade']<=3?'Passed':'Failed'}}"></td>

                        </tr>
                        @endforeach

                        <tr class = "TableHeader" style="text-transform: capitalize"> 
                            <th style="height: 5px;background-color: #007F64;"></th>
                            <?php
                            $count = 0;
                            $end = 'false';
                            ?>
                            @foreach($activitiesData as $data)
                            <?php if ($end == 'false'):
                                ?>
                                <th style="height: 5px;background-color: #007F64;"></th>

                                <?php
                            endif;
                            $end = 'true';
                            ?>
                            @endforeach
                        </tr>
                    </table>
                </div>
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <input type="hidden" name="activitiesNo" value="{{$activitiesNo}}" />
                <input type="hidden" name="recordsNo" value="{{$recordsNo}}" />
                <input type="hidden" name="term" value="{{$term}}" />
                <input type="hidden" name="subject_id" value="{{$subject_id}}" />
            </div>
            <div class = pull-right style="margin-bottom: 15px">
                @if($term=='Pre-Final')
                <button id="export-btn" style="height: 30px;"class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" data-toggle = "modal" data-target = "#myModal5" style  = "height: 30px;" class="btn btn-primary"><span class = "glyphicon glyphicon-file"></span> Final Result</button>
                @elseif($term=='Midterm')
                <button id="export-btn" style="height: 30px;"class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" data-toggle = "modal" data-target = "#myModal6" style  = "height: 30px;" class="btn btn-primary"><span class = "glyphicon glyphicon-file"></span> Midterm Result</button>
                @endif
            </div>
        </form>
        <br>
        <br>
    </div>
    @endif
    <!-- Modal -->
    <form method="post" action="{{ url('/addactivity') }}" id="updateform">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="modal fade" id="myModal" role="dialog">

            <div class="modal-dialog" style="width: 400px;">

                <!-- Modal content-->

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4><span class="glyphicon glyphicon-plus "></span> Add Activity</h4>
                    </div>
                    <div class="modal-body" style="padding:40px 50px;">

                        <div class="form-group col-lg-6">
                            <h5 for="activity"><span class="glyphicon glyphicon-file"></span> Activity Name </h5>

                            <select name="selected_act" style = "line-height: 200px;height: 33px;font-size: 13px;">
                                <option>Select Activity</option>
                                @foreach($act_detail as $data)
                                @if($data['act_name']!='TermExam')
                                @if($data['act_name']!='TermExam-lab')
                                <option>{{$data['act_name']}}</option>
                                @endif
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-lg-6">
                            <h5 for="activity"><span class="glyphicon glyphicon-tasks"></span> Total Score </h5>
                            <input type="text" class="form-control" name="total" placeholder="Total Score">
                        </div>

                    </div>
                    <div class = "modal-footer">
                        <button Data-dismiss ="modal" class="btn btn-success btn-danger"><span class="glyphicon glyphicon-remove"></span>  Cancel</button>
                        <button type="submit" class="btn btn-success btn-success"><span class="glyphicon glyphicon-plus"></span>  Add</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
    <!-- Modal -->

    @include('pages.instructor.final_modal')
    @include('pages.instructor.midterm_modal')
</div>
<script>
    $(document).ready(function () {
    });

</script>