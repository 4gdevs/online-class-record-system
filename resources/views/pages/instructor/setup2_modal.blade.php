<!-- Modal -->

<div class="modal fade" id="myModals2" role="dialog">
    <div class="modal-dialog" style="width:360px">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><span class="glyphicon glyphicon-wrench "></span> Settings</h4>
            </div>

            <div class="modal-body" style = "padding:0px 10px 0px 10px">
                {!! Form::open( array(
                'route' => 'settings.create',
                'method' => 'post',
                'id' => 'form-add-activities'
                ) ) !!}
                <h5 for="activity"><span class="glyphicon glyphicon-file">Name</span>
                    <input id="act_name" name="act_name" style="width: 100px">
                    <select id="selectedtype" name="selectedtype" style = "line-height: 150px;height: 23.5px;font-size: 11px;">
                        <option>Type</option>
                        <option>lecture</option>
                        <option>laboratory</option>
                    </select>
                    {!! Form::submit( 'Add', array(
                    'id' => 'btn-add-activities',
                    ) ) !!}
                    {!! Form::close() !!}
                </h5>
                <form method="post" action="{{ url('/setfav') }}" id="form1" >
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <div class="panel-group" style = "padding:0px 2px 0px 2px;">
                        <table id = "myTable" class = "table table-bordered"  >
                            <tbody>
                                <tr class = "TableHeader"> 
                                    <th>Activity Name</th>
                                    <th>Activity Type</th>
                                </tr>
                                @foreach($set_list as $data)
                                @if($data['name']!='TermExam')
                                @if($data['name']!='TermExam-lab')
                                <tr class="child"><td><input name="{{$data['id']}}" readonly value="{{$data['name']}}" style="border:0"</td><td><input name="{{$data['id']}}" readonly value="{{$data['type']=='lec'?'lecture':'laboratory'}}" style="border:0; width: 100px"</td></tr>
                                @endif
                                @endif
                                @endforeach

                            </tbody>
                        </table>
                    </div>
            </div>
            <div class = "modal-footer">
                <input type="hidden" id="actNo" name="actNo">
                <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>  Discard</button>
                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span>  Save</button>
            </div>
            </form>
        </div>

    </div>
</div>
<script>
    var count = 0;
    $(document).ready(function () {
        $('#form-add-activities').on('submit', function () {
            var act = $('#act_name').val();
            var selectedtype = $('#selectedtype').val();
            if (selectedtype != 'Type') {
                if (act != "") {
                    count = +count + 1;
                    $('#myTable').append('<tr class="child"><td><input name="act' + count + '" readonly value="' + act + '" style="border:0"</td><td><input name="act_type' + count + '" readonly value="' + selectedtype + '" style="border:0; width: 100px"</td></tr>');
                    document.getElementById('act_name').value = "";
                    document.getElementById('actNo').value = count;
                }
            }
            return false;
        });
    });
</script>